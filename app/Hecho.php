<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hecho extends Model
{
    protected $table="p_hechos";

    protected $fillable=["numPremisa","estado","user_id","diag_id","elem_id","sinto_id","context_id","peso_id"];

    /*public function enferminfluyente(){
    	return $this->belongsTo("App\Enferminfluyente","enferm_id");
    }*/

    public function elemento(){
    	return $this->belongsTo("App\Elemento","elem_id");
    }

    public function sintoma(){
    	return $this->belongsTo("App\Sintoma","sinto_id");
    }

    public function contexto(){
    	return $this->belongsTo("App\Contexto","context_id");
    }

    public function pesossintoma(){
        return $this->belongsTo("App\PesosSintoma","peso_id");
    }

    /*public function medicinfluyente(){
    	return $this->belongsTo("App\Medicinfluyente","medic_id");
    }*/

    public function diagnostico(){
    	return $this->belongsTo("App\Diagnostico","diag_id");
    }

    public function user(){
    	return $this->belongsTo("App\User","user_id");
    }

}
