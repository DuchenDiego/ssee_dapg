<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{

    public $timestamps = false;
    
    protected $table="eventos";

    protected $fillable=["title","start","end","rendering","color"];


}
