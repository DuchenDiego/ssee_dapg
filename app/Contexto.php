<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contexto extends Model
{
    protected $table="contextos";

    protected $fillable=["name","tipocontext","pregunta","islikert","opcionlik1","opcionlik2","opcionlik3","opcionlik4","opcionlik5"];

    /*public function users(){
        return $this->belongsToMany("App\User","p_hechos");
    }*/

    public function hechos(){
        return $this->hasMany("App\Hecho","context_id");
    }

    public function criterios(){
    	return $this->hasMany("App\Criterio","context_id");
    }

    /*public function aplicados(){
    	return $this->hasmany("App\Aplicado","predis_id");
    }*/

}
