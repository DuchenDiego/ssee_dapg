<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DiagFinalizado extends Notification
{
    use Queueable;

    protected $diagnostico;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($diagnostico)
    {
        $this->diagnostico=$diagnostico;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    
    public function toArray($notifiable)
    {
        return $this->diagnostico;
        /*return [
            'message'=>'Nuevo caso '
        ];*/
    }
}
