<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Criterio;
use App\Diagnostico;
use App\Elemento;
use App\Sintoma;
use App\Hecho;
use App\PesosSintoma;
use App\Contexto;
use DB;
use Auth;
use Session;

/**Para notificacion */
use App\Personal; 
use App\Notifications\DiagFinalizado;
/** */

class MotorInfController extends Controller
{
    public function verifySintomas($id){
        //verificar si se registraron todos los sitomas del elemento,

        //extraer ultimo diagnostico
        $iduser=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$iduser)->max('numero');
        $diag=Diagnostico::where('user_id','=',$iduser)->where('numero','=',$ultdiag)->first();

        $sintoma=Criterio::where('sinto_id','=',$id)->where('conclusion','=',0)->first();
        $sintoselemento=Criterio::where('premis_id','=',$sintoma->premis_id)->where('conclusion','=',0)->get();

        $contador=0;
        foreach ($sintoselemento as $sint){
            $hechosint=Hecho::where('sinto_id','=',$sint['sinto_id'])->where('user_id','=',$iduser)->where('diag_id','=',$diag->id)->first();
            if($hechosint==true){
                $contador++;
            }else{
                return redirect()->route('show.sintomas', ['idsint'=>$sint['sinto_id']]);  //Mandar idsint para que se muestre 
            }
        }

        if(count($sintoselemento)==$contador){
            $elemento=Criterio::where('premis_id','=',$sintoma->premis_id)->where('conclusion','=',1)->first();
            return redirect()->route('verificar.elementos',['id'=>$elemento->elem_id]);
        }
        
    }

    public function verifyContextosSint($id){
        // verificar la insercion de todas las variables de contexto del sintoma

        //extraer ultimo diagnostico
         $iduser=Auth::user()->id;
         $ultdiag=Diagnostico::where('user_id','=',$iduser)->max('numero');
         $diag=Diagnostico::where('user_id','=',$iduser)->where('numero','=',$ultdiag)->first();

        //extraer los contextos del sintoma
        $sintoma=Sintoma::where('id','=',$id)->first();
        $p_sintoma=PesosSintoma::where('name','=','p_'.$sintoma->name)->first();
        $coincidencias=(Criterio::where('tipo','=','contexto')->where('peso_id','=',$p_sintoma->id)->get())->toArray();// transformacion de obj a array para sacar campo premis_id

            //volcado de ids de premisa a un array aux
        $auxcoincids=[];
        foreach ($coincidencias as $arraycoinc) {
            foreach ($arraycoinc as $keycoinc => $valuecoinc) {
                if ($keycoinc=="premis_id") {
                    array_push($auxcoincids,$valuecoinc);
                }
            }
        }

        foreach ($auxcoincids as $ids) {
            $critcontext=Criterio::where('premis_id','=',$ids)->where('conclusion','=',0)->first();
            $hechocontext=Hecho::where('context_id','=',$critcontext->context_id)->where('user_id','=',$iduser)->where('diag_id','=',$diag->id)->first();

            //si no existe el contexto en hechos redireccionar a la vista relacionada 
            if($hechocontext==false){
        
                return redirect()->route('show.contexto', ['idsint'=>$id,'idcontext'=>$critcontext->context_id]);
            }
        }
        
        return redirect()->route('verificar.pesos',['id'=>$id]);      
    }

    public function verifyPesosSint($id){
        // asignación de pesos para la devaluación del resultado del síntoma rel.

        //extraer ultimo diagnostico
        $iduser=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$iduser)->max('numero');
        $diag=Diagnostico::where('user_id','=',$iduser)->where('numero','=',$ultdiag)->first();

        //extraer sintoma y peso del sintoma
        $sintoma=Sintoma::where('id','=',$id)->first();
        $peso=PesosSintoma::where('name','=','p_'.$sintoma->name)->first();

        $hechosint=Hecho::where("sinto_id","=",$id)->where("user_id","=",$iduser)->where("diag_id","=",$diag->id)->first();
        $hechopeso=Hecho::where("peso_id","=",$peso->id)->where("user_id","=",$iduser)->where("diag_id","=",$diag->id)->first();

        if($hechopeso==true){
            
            //aplicacion de peso a sintoma
            $porcent=100-($hechopeso->estado);//porcentaje a aplicar al valor del sintoma
            $valorfinal= ($porcent*($hechosint->estado))/100; //Regla de tres simple


            Hecho::updateOrCreate(
                ['user_id'=>$iduser, 'numPremisa'=>$hechosint->numPremisa,'diag_id'=>$diag->id, 'sinto_id'=>$id],
                ['estado'=>$valorfinal]
            );
        }
        
        return redirect()->route('verificar.sintomas',['id'=>$id]);
    }

    public function verifyElementos($id){

        //extraer ultimo diagnostico
        $idusr=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$idusr)->max('numero');
        $diag=Diagnostico::where('user_id','=',$idusr)->where('numero','=',$ultdiag)->first();

        $verificar=0;
        $valorelem=0;
        //Elemento determinado
        $elemento=Criterio::where('elem_id','=',$id)->where('conclusion','=',1)->first();
        //Cantidad de sintomas de elemento 
        $sintoma=Criterio::where('premis_id','=',$elemento->premis_id)->where('conclusion','=',0)->get();
        
        foreach ($sintoma as $valsint) {
            $hechosint=Hecho::where('numPremisa','=',$valsint["premis_id"])->where('sinto_id','=',$valsint["sinto_id"])->where('diag_id','=',$diag->id)->first();
            if ($hechosint==true) {
                $verificar++;
                $valorelem+=$hechosint->estado;
            }
        }
        if($verificar==count($sintoma)){

            $valorelemfinal=$valorelem/$verificar;//promedio
            if($valorelemfinal>0 && $valorelemfinal<=20){
                $valorelemfinal=0;
            }else if($valorelemfinal>20 && $valorelemfinal<=40){
                $valorelemfinal=1;
            }else if($valorelemfinal>40 && $valorelemfinal<=60){
                $valorelemfinal=2;
            }else if($valorelemfinal>60 && $valorelemfinal<=80){
                $valorelemfinal=3;
            }else if($valorelemfinal>80 && $valorelemfinal<=100){
                $valorelemfinal=4;
            }
            Hecho::updateOrCreate(
                ['user_id'=>$idusr, 'numPremisa'=>$elemento->premis_id,'diag_id'=>$diag->id, 'elem_id'=>$id],
                ['estado'=>$valorelemfinal]
            );
        }

        return redirect()->route('show.sintomas');

    }

    public function verifyIndicador(){

        //extraer ultimo diagnostico
        $idusr=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$idusr)->max('numero');
        $diag=Diagnostico::where('user_id','=',$idusr)->where('numero','=',$ultdiag)->first();

        $indicador=Hecho::where('elem_id','!=', NULL)->where('user_id','=',$idusr)->where('diag_id','=',$diag->id)->sum('estado');
        $resultado="";

        if($indicador>=0 && $indicador<6){
            $resultado="Sin Ansiedad";
        }else if($indicador>=6 && $indicador<15){
            $resultado="Ansiedad Menor";
        }else if($indicador>=15){ 
            $resultado="Síndrome Ansioso";
        }

        Diagnostico::updateOrCreate(
            ['user_id'=>$idusr, 'id'=>$diag->id, 'numero'=>$diag->numero],
            ['indicador'=>$indicador, 'resultado'=>$resultado]
        );

        /**NOTIFICACION AL PERSONAL */
        if ($resultado!="Sin Ansiedad"){
            $estudiante=User::where('id','=',$idusr)->first();
            $diagnostico=['idestud'=>$idusr,'iddiag'=>$diag->id,'cred'=>$estudiante->idcredencial];

            $encargados=Personal::all();
            foreach($encargados as $encargado){
                $encargado->notify(new DiagFinalizado($diagnostico));
            }
        }
        /** */

        if($resultado=="Síndrome Ansioso"){
            return redirect()->route('show.sintTras');
        }

        return redirect()->route('resultado');
    }

    public function verifySintTras(){//pendiente revision

        
        //extraer ultimo diagnostico
        $idusr=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$idusr)->max('numero');
        $diag=Diagnostico::where('user_id','=',$idusr)->where('numero','=',$ultdiag)->first();

        $sintTras=Sintoma::where('sinttras','=',1)->get();
        $hechosintTras=Hecho::where('numPremisa','=',30)->where('user_id','=',$idusr)->where('diag_id','=',$diag->id)->get()->toArray();

        
        $arraux=[];
        $auxcerocounter=0;
        if (count($sintTras)==count($hechosintTras)) {
            foreach ($hechosintTras as $keyhechsintt => $valuehechsintt) {
                $arraux+=[$valuehechsintt['sinto_id']=>$valuehechsintt['estado']];
                if($valuehechsintt['estado']==0){
                    $auxcerocounter++;
                }
            }
            if($auxcerocounter==8){
                $diag->tipotrastorno="Indefinido";
                $diag->save();
            }
            else if ($arraux[28]>0 && $arraux[22]>0) {
                $diag->tipotrastorno="TAG";
                $diag->save();
            }
            else if ($arraux[29]>0 && $arraux[30]>0) {
                $diag->tipotrastorno="TAS";
                $diag->save();
            }
            else if ($arraux[31]>0 && $arraux[20]>0) {
                $diag->tipotrastorno="TP";
                $diag->save();
            }
            else if ($arraux[33]>0 && $arraux[34]>0){
                $diag->tipotrastorno="TOC";
                $diag->save();
            }else{
                $diag->tipotrastorno="TAG";
                $diag->save();
            }
        } else {
            return redirect()->route('show.sintTras');
        }
                
        return redirect()->route('resultado');
    }
}
