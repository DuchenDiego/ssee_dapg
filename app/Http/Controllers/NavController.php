<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Criterio;
use App\Diagnostico;
use App\Elemento;
use App\Sintoma;
use App\Hecho;
use App\PesosSintoma;
use App\Contexto;
use App\Calendario;
use Session;
use Auth;

class NavController extends Controller
{
    /** definicion de sesiones para navegacion(pendiente) */
    public function backRequests(){

        //Session::put('navBack',"Texto de prueba funciona");

        //dd(url()->previous());
        
    }

    public function nextRequests(){


        // $navback=Session::get('navBack');

        // dd($navback);
    }

    public function calGetevents(){

        $eventos=Calendario::all()->toArray();

        return view('personal/calendario/calendario',['eventos'=>$eventos]);
    }

    public function calNewevent(Request $request){

        //dd($request->all());

        // Calendario::updateOrCreate(
        //     ['title'=>$request->titulo],
        //     ['start'=>$request->fecha_inicio,'end'=>$request->fecha_final,'color'=>"#2C736B"]
        // );

        $eve=new Calendario;
        $eve->title=$request->titulo;
        $eve->start=$request->fecha_inicio;
        $eve->end=$request->fecha_final;
        $eve->color="#2C736B";
        $eve->save();

        return redirect(route('calget'));
    }

    public function calDropevent($idevent){

        $event=Calendario::where('id','=',$idevent)->first();

        Calendario::destroy($event->id);

        return redirect(route('calget'));

    }
}
