<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diagnostico;
use App\User;
use App\Hecho;
use App\Medicinfluyente;
use App\Sintoma;
use App\Predisposicion;
use Session;
use PDF;

class PDFController extends Controller
{
    public function pdf(){

		$pdfdata=Session::get('pdfdata');
		$pdfdatauser=Session::get('pdfdatauser');
		$pdfresultgabin=Session::get('pdfresultgabin');

    	$pdf=PDF::loadview('personal/ModuloInterpretacion/pdf', compact('pdfdata','pdfdatauser','pdfresultgabin'));
    	$pdf->setPaper('legal', 'portrait'); //or landscape
		return $pdf->stream('Detalle.pdf');
		
		// return view('personal/ModuloInterpretacion/pdf', compact('pdfdata','pdfdatauser','pdfresultgabin'));
    }
}
