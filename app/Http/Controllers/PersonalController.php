<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personal;
use App\PesosSintoma;
use App\Contexto;
use App\Elemento;
use App\Sintoma;
use App\Criterio;
use DB;
use Auth;
use Session;


class PersonalController extends Controller
{
    /*Controlador creado a base de una duplicacion del archivo "HomeController"*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /*Para que llame al guardia respectivo*/
        $this->middleware('auth:personalguard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     /**Funciones de Redirección */
    public function index()
    {
        Session::put('superuser', Auth::user()->Su);
        Session::put('idpers', Auth::user()->id);
        return view('personal/inicio/modulos');
    }
    public function interpret()
    {
        return view('personal/ModuloInterpretacion/contentbusq');
    }
    
    /**Funciones de Gestión de Personal */
    public function gestionpers($idpers=0,$confirmreg=0)
    {
        $personal=Personal::all();
        $selectedpers=NULL;
        if($idpers!=0){
            $selectedpers=Personal::where('id','=',$idpers)->first();
        }
        return view('personal/ModuloInterpretacion/gestionpers',['personal'=>$personal,'selectedpers'=>$selectedpers,'confirmreg'=>$confirmreg]);
    }
    public function regispers(Request $request){

        $superusr=0;
        if($request["superus"]){
            $superusr=1;
        }
        Personal::updateOrCreate(
            ['id'=>$request['id']],
            ['name'=>$request['usuario'],'password'=>bcrypt($request['password']),'Su'=>$superusr]
        );
        $confirmreg=1;
        return redirect()->route('gestionpers',['idpers'=>0,'confirmreg'=>$confirmreg]);
    }
    public function bajapers($idpers,$opcion){

        if (!$opcion) {
            $personal=Personal::where('id','=',$idpers)->first();
            $personal->baja=1;
            $personal->save();    
        }else{
            $personal=Personal::where('id','=',$idpers)->first();
            $personal->baja=0;
            $personal->save();
        }

        return redirect()->route('gestionpers',['idpers'=>0,'confirmreg'=>0]);
    }

    /**FUnciónes para Gestión de pesos */
    public function visualreglas($confirmupd=false)
    {
        $raw_contextos=DB::select('SELECT codigo,premis_id,tipo,conclusion,name
        FROM reglas,premisas,p_criterios,contextos
        WHERE premisas.regla_id=reglas.id AND
              p_criterios.premis_id=premisas.id AND
              context_id=contextos.id', [1]);

        $raw_pesos=DB::select('SELECT codigo,premis_id,tipo,conclusion,valor, name
        FROM reglas,premisas,p_criterios,pesos_sintomas
        WHERE premisas.regla_id=reglas.id AND
              p_criterios.premis_id=premisas.id AND
              peso_id=pesos_sintomas.id',[1]);

        $raw_sintomas=DB::select('SELECT codigo,premis_id,tipo,conclusion, name
        FROM reglas,premisas,p_criterios,sintomas
        WHERE premisas.regla_id=reglas.id AND
              p_criterios.premis_id=premisas.id AND
              sinto_id=sintomas.id
        ',[1]);

        $raw_elementos=DB::select('SELECT codigo,premis_id,tipo,conclusion, name
        FROM reglas,premisas,p_criterios,elementos
        WHERE premisas.regla_id=reglas.id AND
              p_criterios.premis_id=premisas.id AND
              elem_id=elementos.id',[1]);
        
        $reglasContexto=[];//array final Contextos

        $ContextoPesos=array_merge($raw_contextos,$raw_pesos); //Juntar array para reglas

        foreach ($ContextoPesos as $valueCP) { //Agrupar cada Peso/Contexto por regla
            $reglasContexto[$valueCP->codigo][]=$valueCP;
        }

        $reglasElemento=[];//array final Elementos

        $ElementoSintomas=array_merge($raw_sintomas,$raw_elementos);

        foreach ($ElementoSintomas as $valueES) {
            $reglasElemento[$valueES->codigo][]=$valueES;
        }

        return view('personal/ModuloConocimiento/arbolreglas',['Contextos'=>$reglasContexto,'Elementos'=>$reglasElemento,'confirmupd'=>$confirmupd]);
    }

    public function gestionreglas(Request $request)
    {
        $pesos=$request->all();
        
        foreach ($pesos as $keypeso => $valuepeso) {

            if($keypeso!="_token" && $keypeso!="premis_id"){

                $getidpeso=PesosSintoma::where("name","=",$keypeso)->first();
                $verifpeso=Criterio::where("premis_id","=",$pesos["premis_id"])->where("peso_id","=",$getidpeso->id)->first();

                if($verifpeso){
                    $verifpeso->valor=$valuepeso;
                    $verifpeso->save();
                }
            }
        }
        $confirmupd=true; //confirmacion de mod

        return redirect()->route('reglas.visual',['confirmupd'=>$confirmupd]);
    }
}
