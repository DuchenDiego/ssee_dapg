<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Criterio;
use App\Diagnostico;
use App\Elemento;
use App\Sintoma;
use App\Hecho;
use App\PesosSintoma;
use App\Contexto;
use Auth;
use Session;

class ShowController extends Controller
{
    public function showSintomas($idsint=0){ // idsint inicializado en 0 si es que no se recibe sintoma para mostrar

        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $diag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();

       /**NAVEGACION */

       /** */

        if($idsint==0){
            $sintomas=Sintoma::where("sinttras","=",0)->get();//Revision de ultimos(sinttras) sintomas pendiente
        }else{
            $sintomas=Sintoma::where("sinttras","=",0)->where("id","=",$idsint)->get();
        }


        foreach($sintomas as $campos){
                $hechosint=Hecho::where("sinto_id","=",$campos["id"])->where("user_id","=",$id)->where("diag_id","=",$diag->id)->first();
                if($hechosint==false){
                    return view("deteccion/ModuloDeteccion/criterios/pregsint_template")
                        ->with("sintid",$campos["id"])
                        ->with("preg",$campos["pregunta"])
                        ->with("likert",$campos["islikert"])
                        ->with("opc1",$campos["opcionlik1"])
                        ->with("opc2",$campos["opcionlik2"])
                        ->with("opc3",$campos["opcionlik3"])
                        ->with("opc4",$campos["opcionlik4"])
                        ->with("opc5",$campos["opcionlik5"]);
                }
            }           

        return redirect()->route('verificar.indicador');
    }
    
    public function showContexto($idsint,$idcon){


        $context=Contexto::where('id','=',$idcon)->first();
        
        return view("deteccion/ModuloDeteccion/criterios/pregcontext_template")
                    ->with("contextid",$context->id)
                    ->with("sintid",$idsint)
                    ->with("preg",$context->pregunta)
                    ->with("likert",$context->islikert)
                    ->with("opc1",$context->opcionlik1)
                    ->with("opc2",$context->opcionlik2)
                    ->with("opc3",$context->opcionlik3)
                    ->with("opc4",$context->opcionlik4)
                    ->with("opc5",$context->opcionlik5);
    }

    public function showSintTrastorno(){
        
        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $diag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();

        $sintomasT=Sintoma::where("sinttras","=",1)->get();

        foreach ($sintomasT as $sintTras) {

            $hechosintT=Hecho::where("sinto_id","=",$sintTras["id"])->where("user_id","=",$id)->where("diag_id","=",$diag->id)->first();
            if (!$hechosintT) {
                return view("deteccion/ModuloDeteccion/criterios/pregsintTras_template")
                        ->with("sintid",$sintTras["id"])
                        ->with("preg",$sintTras["pregunta"])
                        ->with("likert",$sintTras["islikert"])
                        ->with("opc1",$sintTras["opcionlik1"])
                        ->with("opc2",$sintTras["opcionlik2"])
                        ->with("opc3",$sintTras["opcionlik3"])
                        ->with("opc4",$sintTras["opcionlik4"])
                        ->with("opc5",$sintTras["opcionlik5"]);
            }
        }
    }

    
}
