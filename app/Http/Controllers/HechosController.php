<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Criterio;
use App\Hecho;
use App\Diagnostico;
use App\Sintoma;
use App\Contexto;
use App\PesosSintoma;
use Auth;

class HechosController extends Controller
{
    public function hechosSintomas(Request $request){
        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $diag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();
        $crisint=Criterio::where('sinto_id','=',$request->sintid)->where('tipo','=','sintoma')->first();


        if($request->likert==1){

            $arrayopc=[
                10 => Input::has('op1'),
                30 => Input::has('op2'),
                50 => Input::has('op3'),
                70 => Input::has('op4'),
                90 => Input::has('op5')

                // 10 => false,
                // 30 => false,
                // 50 => false,
                // 70 => true,
                // 90 => false
            ];

        }else{

            $arrayopc=[
                100 => Input::has('si'),
                0   => Input::has('no')

                //  100 => true,
                //  0   => false
            ];

        }

        $truecounter=0;
    
        //Si se selecciona más de una opcion
        foreach($arrayopc as $opc){
            
            if ($opc==true){
                $truecounter++;
            }
        }
        if($truecounter>1 || $truecounter==0){
            return redirect()->back();
        }

        // Escenarios
        foreach ($arrayopc as $keyopc2 => $valopc2) {

            $valorsint=$crisint->valor;

            if($valopc2==true){

                Hecho::updateOrCreate(
                    ['user_id'=>$id, 'numPremisa'=>$crisint->premis_id,'diag_id'=>$diag->id, 'sinto_id'=>$request->sintid],
                    ['estado'=>$keyopc2]
                );

                if(($crisint->comparador==">=" && $keyopc2>=$valorsint)
                || ($crisint->comparador=="<=" && $keyopc2<=$valorsint)
                || ($crisint->comparador=="==" && $keyopc2==$valorsint)){
                    // si se cumple la condicion de sintoma verificar las variables de contexto de ese sintoma
                    
                    return redirect()->route('verificar.contextos',['id'=>$request->sintid]);
                }
            }
        }
        
        return redirect()->route('verificar.sintomas',['id'=>$request->sintid]);
       
    }

    public function hechosContexto(Request $request){
        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $diag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();
        $cricontext=Criterio::where('context_id','=',$request->contextid)->where('tipo','=','contexto')->first();

        if($request->likert==1){

            $arrayopc=[
                10 => Input::has('op1'),
                30 => Input::has('op2'),
                50 => Input::has('op3'),
                70 => Input::has('op4'),
                90 => Input::has('op5')
            ];

        }else{

            $arrayopc=[
                100 => Input::has('si'),
                0   => Input::has('no')
            ];
        }
            $truecounter=0;

            //Si se selecciona más de una opcion
            foreach($arrayopc as $opc){
                
                if ($opc==true){
                    $truecounter++;
                }
            }
            if($truecounter>1 || $truecounter==0){
                return redirect()->back();
            }

            // Escenarios
            foreach ($arrayopc as $keyopc2 => $valopc2) {

                $valorcontext=$cricontext->valor;

                if($valopc2==true){

                    Hecho::updateOrCreate(
                        ['user_id'=>$id, 'numPremisa'=>$cricontext->premis_id,'diag_id'=>$diag->id, 'context_id'=>$request->contextid],
                        ['estado'=>$keyopc2]
                    );

                    if(($cricontext->comparador==">=" && $keyopc2>=$valorcontext) 
                    || ($cricontext->comparador=="<=" && $keyopc2<=$valorcontext)
                    || ($cricontext->comparador=="==" && $keyopc2==$valorcontext)){
                        // si se cumple la condicion de contexto, insertar o modificar los pesos de los sintomas relacionados
                        $pesosrel=Criterio::where('premis_id','=',$cricontext->premis_id)->where('tipo','=','contexto')->where('conclusion','=',1)->get();
                        
                        foreach ($pesosrel as $valuepeso) {

                            $hechopeso=Hecho::where('peso_id','=',$valuepeso['peso_id'])->where('user_id','=',$id)->where('diag_id','=',$diag->id)->first();

                            if($hechopeso==false){// si no existe el peso del contexto en los hechos, crearlo  con su valor
                                Hecho::updateOrCreate(
                                    ['user_id'=>$id, 'numPremisa'=>$valuepeso['premis_id'],'diag_id'=>$diag->id, 'peso_id'=>$valuepeso['peso_id']],
                                    ['estado'=>$valuepeso['valor']]
                                );
                            }else{
                                Hecho::updateOrCreate(// si existe el peso del contexto en los hechos, sumarle el nuevo valor y cambiarle el id de premisa
                                    ['user_id'=>$id,'diag_id'=>$diag->id, 'peso_id'=>$valuepeso['peso_id']],
                                    ['estado'=>$hechopeso->estado+$valuepeso['valor'],'numPremisa'=>$valuepeso['premis_id']]
                                );
                            }
                            
                            //Re-definicion del hecho para actualizacion de campo "estado"
                            $hechopeso=Hecho::where('peso_id','=',$valuepeso['peso_id'])->where('user_id','=',$id)->where('diag_id','=',$diag->id)->first();
                            if(($hechopeso->estado)>100){// si la suma del peso añadido sobrepasa los 100, mantenerlo en 100

                                Hecho::updateOrCreate(
                                    ['user_id'=>$id,'diag_id'=>$diag->id, 'peso_id'=>$valuepeso['peso_id']],
                                    ['estado'=>100,'numPremisa'=>$valuepeso['premis_id']]
                                );  
                            }
                        }
                    }
                }
            }

            return redirect()->route('verificar.contextos',['id'=>$request->sintid]);
    }



    public function hechosSintomasT(Request $request){
        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $diag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();

        if($request->likert==1){

            $arrayopc=[
                10 => Input::has('op1'),
                30 => Input::has('op2'),
                50 => Input::has('op3'),
                70 => Input::has('op4'),
                90 => Input::has('op5')

            ];

        }else{

            $arrayopc=[
                100 => Input::has('si'),
                0   => Input::has('no')
            ];

        }

        $truecounter=0;
    
        //Si se selecciona más de una opcion
        foreach($arrayopc as $opc){
            
            if ($opc==true){
                $truecounter++;
            }
        }
        if($truecounter>1 || $truecounter==0){
            return redirect()->back();
        }

        // Escenarios
        foreach ($arrayopc as $keyopc2 => $valopc2) {

            if($valopc2==true){

                Hecho::updateOrCreate(
                    ['user_id'=>$id, 'numPremisa'=>30,'diag_id'=>$diag->id, 'sinto_id'=>$request->sintid],
                    ['estado'=>$keyopc2] //hardcode de criterio ficticio(pendiente borrado)
                );
            }
        }
        
        return redirect()->route('verificar.sintomasT');
    }
}
