<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Criterio;
use App\Diagnostico;
use App\Elemento;
use App\Sintoma;
use App\Contexto;
use App\PersonalResult;
use Auth;
use Session;

use App\Hecho;

class ResultadoController extends Controller
{
    public function resultado(){
    	$user=Auth::user();
    	$ultdiag=Diagnostico::where('user_id','=',$user->id)->max('numero');
    	$diag=Diagnostico::where('user_id','=',$user->id)->where('numero','=',$ultdiag)->first();

    	return view("deteccion/ModuloDeteccion/listados/resultado")->with('user',$user)->with('diag',$diag);

    }

    public function detalle($iddiag){
    	$medicdiag=Hecho::where('diag_id','=',$iddiag)->whereNotNull('medic_id')->where('estado','=',1)->orderBy('id','ASC')->get();
    	$sintodiag=Hecho::where('diag_id','=',$iddiag)->whereNotNull('sinto_id')->where('estado','=',1)->orderBy('id','ASC')->get();
    	$preddiag=Hecho::where('diag_id','=',$iddiag)->whereNotNull('predis_id')->where('estado','=',1)->orderBy('id','ASC')->get();

    	$arrmedic=array();
    	$arrsinto=array();
    	$arrpred=array();

    	foreach ($medicdiag as $med) {
    		$medic=Medicinfluyente::where("id","=",$med["medic_id"])->first();
    		array_push($arrmedic,$medic->name);
    	}
    	foreach($sintodiag as $sint){
    		$sinto=Sintoma::where("id","=",$sint["sinto_id"])->first();
    		array_push($arrsinto,$sinto->name);
    	}
    	foreach ($preddiag as $pred) {
    		$predis=Predisposicion::where("id","=",$pred["predis_id"])->first();
    		array_push($arrpred,$pred->name);
    	}

    	return view("deteccion/ModuloDeteccion/listados/detalle")->with('medic',$arrmedic)->with('sinto',$arrsinto)->with('predis',$arrpred);
    }

    public function historial(){
    	$user=Auth::user();
    	$diag=Diagnostico::where('user_id','=',$user->id)->get();
    	return view("deteccion/ModuloDeteccion/listados/historial")->with('user',$user)->with('diag',$diag);
	}
	
	/**Funciones para búsqueda */

    public function busqueda_espec(Request $request){
	
		$querybusq=User::query();
		if($request['togglecred']) {$querybusq=$querybusq->where('idcredencial','like',$request['credencial']);}
		if($request['togglenac']) {$querybusq=$querybusq->where('fechanac','like',$request['nacfech']);}
		if($request['togglecar']) {$querybusq=$querybusq->where('carrera','like',$request['campocar1']);}
		if($request['togglesem']) {$querybusq=$querybusq->where('semestre','like',$request['camposem1']);}
		$busqueda=$querybusq->get();
		if(!$request['togglecred']&&!$request['togglenac']&&!$request['togglecar']&&!$request['togglesem']){$busqueda=NULL;}

		$arraux=array();
		
		if($busqueda!=NULL){
			foreach ($busqueda as $bus ) {
				$diaggus=Diagnostico::where('user_id','=', $bus["id"])->get();
				$arritem=array(	'idusr'=>$bus["id"],
								'credencial' =>$bus["idcredencial"],
								'carrera'=>$bus["carrera"],
								'semestre'=>$bus["semestre"],
								'fechanac'=>$bus["fechanac"],
								'numdiag'=>count($diaggus),
								);
				array_push($arraux, $arritem);
			}  
		}
    	return view("personal/ModuloInterpretacion/busqueda")->with('busq',$arraux);
    }

    public function busqueda_gen(Request $request){
    	$arraux=array();
    	if($request->campo=="General"){
    		$usr=User::orderBy('idcredencial')->get();
    	}else if($request->campo=="Carrera"){
    		$usr=User::where('carrera','=',$request->campocar2)->orderBy('carrera','DESC')->get();
    	}else if($request->campo=="Semestre"){
    		$usr=User::where('semestre','=',$request->camposem2)->orderBy('semestre','ASC')->get();
		}
 		foreach ($usr as $us ) {
			$diaggus=Diagnostico::where('user_id','=', $us["id"])->get();
			$arritem=array(	'idusr'=>$us["id"],
							'credencial' =>$us["idcredencial"],
							'carrera'=>$us["carrera"],
							'semestre'=>$us["semestre"],
							'fechanac'=>$us["fechanac"],
							'numdiag'=>count($diaggus),
							);
			array_push($arraux, $arritem);
		}  
    	return view("personal/ModuloInterpretacion/busqueda")->with('busq',$arraux);		
    }

    public function detallepers($estud){
		$datauser=User::where('id','=',$estud)->first();

		$diags=Diagnostico::where('user_id','=',$estud)->get();

    	return view("personal/ModuloInterpretacion/detallepers")->with('confirmchange',null)->with('diags',$diags)->with('datauser',$datauser);
	}
	
	public function detallecaso($estud,$iddiag){
		$datauser=User::where('id','=',$estud)->first();

		$diags=Diagnostico::where('user_id','=',$estud)->get();

		$di=Diagnostico::where('id','=',$iddiag)->first();
        $diag=array();
		$elems=array();
		$sints=array();
		$sintTras=array();
		$contexs=array();
		$hechos=Hecho::where('diag_id','=',$iddiag)->get();
		foreach($hechos as $valhecho){
			if($valhecho["elem_id"]!=NULL){//volcado de todos los hechos de elementos

				$elem=Elemento::where('id','=',$valhecho["elem_id"])->first();
				$elems+=[$elem->name=>$valhecho["estado"]];
				$diag["elementos"]=$elems;

			}else if( $valhecho["sinto_id"]!=NULL && (Sintoma::where('id','=',$valhecho["sinto_id"])->first()->sinttras)!=1){//volcado de todos los hechos de síntomas

				$sint=Sintoma::where('id','=',$valhecho["sinto_id"])->first();

				$sints[$sint->name]=array("valor"=>$valhecho["estado"]);
				$sints[$sint->name]+=["pregunta"=>$sint->pregunta];
				if($sint->islikert){
					$sints[$sint->name]+=["opciones"=>["opc1"=>$sint->opcionlik1,
											"opc2"=>$sint->opcionlik2,
											"opc3"=>$sint->opcionlik3,
											"opc4"=>$sint->opcionlik4,
											"opc5"=>$sint->opcionlik5
											]];
				}

				$diag["sintomas"]=$sints;

			}else if($valhecho["context_id"]!=NULL){//volcado de todas las variables de contexto

				$contex=Contexto::where('id','=',$valhecho["context_id"])->first();
				
				$contexs[$contex->name]=array("valor"=>$valhecho["estado"]);
				$contexs[$contex->name]+=["pregunta"=>$contex->pregunta];
				if($contex->islikert){
					$contexs[$contex->name]+=["opciones"=>["opc1"=>$contex->opcionlik1,
												"opc2"=>$contex->opcionlik2,
												"opc3"=>$contex->opcionlik3,
												"opc4"=>$contex->opcionlik4,
												"opc5"=>$contex->opcionlik5
												]];
				}

				$diag["contextos"]=$contexs;
				
			}else if($valhecho["sinto_id"]!=NULL && (Sintoma::where('id','=',$valhecho["sinto_id"])->first()->sinttras)==1){

				$sintt=Sintoma::where('id','=',$valhecho["sinto_id"])->first();

				$sintTras[$sintt->name]=array("valor"=>$valhecho["estado"]);
				$sintTras[$sintt->name]+=["pregunta"=>$sintt->pregunta];
				if($sintt->islikert){
					$sintTras[$sintt->name]+=["opciones"=>["opc1"=>$sintt->opcionlik1,
											"opc2"=>$sintt->opcionlik2,
											"opc3"=>$sintt->opcionlik3,
											"opc4"=>$sintt->opcionlik4,
											"opc5"=>$sintt->opcionlik5
											]];
				}

				$diag["sintomasT"]=$sintTras;
			}
		}
		

		$diag["resultado"]=$di["resultado"];
		$diag["tipotrastorno"]=$di["tipotrastorno"];
		$diag["iddiag"]=$di["id"];
		$diag["fecha"]=$di["fecha"];
		$diag["hora"]=$di["hora"];
		
		Session::put('pdfdata',$diag);
		Session::put('pdfdatauser',$datauser);
		
		Session::put('diags',$diags);

		$resultgabin=PersonalResult::where('diag_id','=',$iddiag)->first();
		
		if($resultgabin){
			Session::put('pdfresultgabin',$resultgabin);
		}else{
			Session::put('pdfresultgabin',NULL);
		}

		return view("personal/ModuloInterpretacion/detallecaso")->with('data',$diag)->with('resgabin',$resultgabin)->with('datauser',$datauser)->with('diags',$diags)->with('confirmchange',null);
	}
	
	public function cru_resgabin(Request $request){

		$resultgabin=PersonalResult::where('diag_id','=',$request->iddiag)->first();

		$datauser=Session::get('pdfdatauser');
		$diag=Session::get('pdfdata');
		$diags=Session::get('diags');

		$pers=Session::get('idpers');

		PersonalResult::updateOrCreate(
			['diag_id'=>$request->iddiag],
			['notasfinales'=>$request->observaciones,'resultexperto'=>$request->resultgabin, 'pers_id'=>$pers]
		);


		//$resultgabin=PersonalResult::where('diag_id','=',$request->iddiag)->first();

		return view("personal/ModuloInterpretacion/detallepers")->with('confirmchange',"mod")->with('datauser',$datauser)->with('diags',$diags);
	}

	public function del_resgabin($iddiag){

		$resultgabin=PersonalResult::where('diag_id','=',$iddiag)->first();

		$datauser=Session::get('pdfdatauser');
		$diag=Session::get('pdfdata');
		$diags=Session::get('diags');

		PersonalResult::destroy($resultgabin->id);

		//$resultgabin=PersonalResult::where('diag_id','=',$iddiag)->first();

		return view("personal/ModuloInterpretacion/detallepers")->with('confirmchange',"baja")->with('datauser',$datauser)->with('diags',$diags);
	}
}
