<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diagnostico;
use App\Calendario;
use Auth;

class InicioController extends Controller
{
	public function inicio(){
		$id=Auth::user()->id;
        $isDiag=Diagnostico::where('user_id','=',$id)->first();

        //validacion por fecha

        $hoy =strtotime(date("Y-m-d")); 
        $eventos=Calendario::where('title','=','Llenado de Casos')->get();

        $proximo=[];

        foreach ($eventos as $keyeve => $valueeve) {
            if(($hoy>=strtotime($valueeve["start"])) && $hoy<=(strtotime($valueeve["end"]))){
                if($isDiag==true){
                    return view("deteccion/inicio/old");
                }else{
                    return view("deteccion/inicio/new");
                }                
            }
                
            if (($hoy<strtotime($valueeve["start"]))) {
                $proximo=["start"=>$valueeve["start"],"end"=>$valueeve["end"]];
            }
        }
        
        if($isDiag==true){
            return view("deteccion/inicio/old",["restriccion"=>1,"proximo"=>$proximo]);
        }else{
            return view("deteccion/inicio/new",["restriccion"=>1,"proximo"=>$proximo]);
        }
	}

    public function  newDiagnostico(){

        //Si el resultado del ultimo diagnostico es indefinido no se crea un nuevo diagnostico y se continua con ese
        $id=Auth::user()->id;
        $ultdiag=Diagnostico::where('user_id','=',$id)->max('numero');
        $lastdiag=Diagnostico::where('user_id','=',$id)->where('numero','=',$ultdiag)->first();
        if($lastdiag==false || $lastdiag->resultado!="indefinido"){
            $fecha=date("Y-m-d");
            $hora=date("H:i:s");
            $num=Diagnostico::where('user_id','=',$id)->count();
            $diag=new Diagnostico;
            $diag->numero=$num+1;
            $diag->indicador=0;
            $diag->resultado="indefinido";
            $diag->fecha=$fecha;
            $diag->hora=$hora;
            $diag->user_id=$id;
            $diag->save();
        }

          return redirect()->route('show.sintomas');
    }

}
