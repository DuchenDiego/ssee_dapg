<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalResult extends Model
{
    protected $table="p_personal";

    protected $fillable=["notasfinales", "resultexperto", "diag_id", "pers_id"];

    public function personal(){
        return $this->belongsTo("App\Personal","pers_id");
    }

    public function diagnostico(){
        return $this->belongsTo("App\Diagnostico","diag_id");
    }
}
