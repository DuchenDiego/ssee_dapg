<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criterio extends Model
{
    protected $table="p_criterios";

    protected $fillable=["premis_id","tipo","conclusion","elem_id","sinto_id","context_id","peso_id","comparador","valor"];

    public function premisa(){
    	return $this->belongsTo("App\Premisa","premis_id");
    }

    public function elemento(){
    	return $this->belongsTo("App\Elemento","elem_id");
    }

    public function sintoma(){
    	return $this->belongsTo("App\Sintoma","sinto_id");
    }

    public function contexto(){
    	return $this->belongsTo("App\Contexto","context_id");
    }

    public function pesossintoma(){
        return $this->belongsTo("App\PesosSintoma","peso_id");
    }
}
