<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesosSintoma extends Model
{
    protected $table="pesos_sintomas";

    protected $fillable=["name"];

    public function hechos(){
        return $this->hasMany("App\Hecho","peso_id");
    }

    public function criterios(){
    	return $this->hasMany("App\Criterio","peso_id");
    }
}
