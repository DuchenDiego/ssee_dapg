<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePCriteriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_criterios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('premis_id')->unsigned();
            $table->foreign('premis_id')->references('id')->on('premisas')->onDelete('cascade');
            $table->enum('tipo',['elemento','sintoma','contexto']);
            $table->tinyInteger('conclusion')->default(0);
            $table->integer('elem_id')->unsigned()->nullable($value = true);
            $table->integer('sinto_id')->unsigned()->nullable($value = true);
            $table->integer('peso_id')->unsigned()->nullable($value = true);
            $table->integer('context_id')->unsigned()->nullable($value = true);
            $table->foreign('elem_id')->references('id')->on('elementos')->onDelete('cascade');
            $table->foreign('sinto_id')->references('id')->on('sintomas')->onDelete('cascade');
            $table->foreign('peso_id')->references('id')->on('pesos_sintomas')->onDelete('cascade');
            $table->foreign('context_id')->references('id')->on('contextos')->onDelete('cascade');
            $table->string('comparador');
            $table->integer('valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_criterios');
    }
}
