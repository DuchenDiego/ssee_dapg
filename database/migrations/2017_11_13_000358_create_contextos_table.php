<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContextosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contextos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tipocontext');
            $table->string('pregunta');
            $table->tinyInteger('islikert')->default(0);
            $table->string('opcionlik1')->nullable($value = true);
            $table->string('opcionlik2')->nullable($value = true);
            $table->string('opcionlik3')->nullable($value = true);
            $table->string('opcionlik4')->nullable($value = true);
            $table->string('opcionlik5')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contextos');
    }
}
