<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSintomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sintomas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('sinttras')->default(0);
            $table->string('pregunta');
            $table->tinyInteger('islikert')->default(0);
            $table->string('opcionlik1')->nullable($value = true);
            $table->string('opcionlik2')->nullable($value = true);
            $table->string('opcionlik3')->nullable($value = true);
            $table->string('opcionlik4')->nullable($value = true);
            $table->string('opcionlik5')->nullable($value = true);
            //$table->integer('elem_id')->unsigned(); 
            //$table->foreign('elem_id')->references('id')->on('elementos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sintomas');
    }
}
