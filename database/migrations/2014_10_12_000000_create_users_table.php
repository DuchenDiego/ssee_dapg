<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idcredencial')->unique();
            $table->string('password');
            $table->enum('carrera',['Ing_Sistemas','Ing_Electrónica','Ing_Telecomunicaciones',
                                    'Ing_Industrial', 'Ing_Biomédica', 'Medicina', 'Odontología',
                                    'Bioquímica', 'Fisioterapia', 'Arquitectura', 'Turismo',
                                    'Gastronomía', 'Psicología', 'Ing_Petrolera', 'Ing_Civil',
                                    'Admin_Empresas', 'Comercio_Int', 'Ing_Comercial', 'Derecho',
                                    'Contaduría_Pública', 'Ing_Financiera']);
            $table->enum('semestre',['1','2','3','4','5','6','7','8','9','10'])->default('1');
            $table->date('fechanac');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
