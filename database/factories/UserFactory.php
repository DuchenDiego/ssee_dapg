<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        //'name' => $faker->name,
        //'email' => $faker->unique()->safeEmail,
        'id'=> '999',
        'idcredencial'=> 'zmd2017049',
        'password' => $password ?: $password = bcrypt('secret'),
        'carrera'=> 'Ing_Sistemas',
        'semestre'=> '10',
        'fechanac'=> '1989-10-15',
        'remember_token' => str_random(10),
    ];
});
