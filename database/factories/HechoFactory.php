<?php

use Faker\Generator as Faker;

$factory->define(App\Hecho::class, function (Faker $faker) {
    return [
        'id' => $faker->randomDigit,
        'estado'=>100,
        'user_id'=>55,
        'diag_id'=>$faker->randomDigit,
        'sinto_id'=>$faker->randomDigit
    ];
});
