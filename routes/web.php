<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('home');
});*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('pdf/','PDFController@pdf')->name('pdf');

Route::prefix('personal')->group(function(){
	/**Autenticacion */
	Route::get('/login','Auth\PersonalLoginController@showLoginForm')->name('personal.login');
	Route::post('/login','Auth\PersonalLoginController@login')->name('personal.login.submit');
	Route::get('/', 'PersonalController@index')->name('personal.dashboard');

	/**Busqueda */
	Route::get('/interpretacion','PersonalController@interpret')->name('interpret');
	Route::post('/busqueda/espec', 'ResultadoController@busqueda_espec')->name('busqueda.espec');
	Route::post('/busqueda/gen', 'ResultadoController@busqueda_gen')->name('busqueda.gen');
	Route::get('/detalle/{idusr}', 'ResultadoController@detallepers')->name('detallepers');
	Route::get('detalle/caso/{idusr}/{iddiag}','ResultadoController@detallecaso')->name('detallecaso');
	Route::post('detalle/caso/resgabin','ResultadoController@cru_resgabin')->name('resgabin');
	Route::get('detalle/caso/resgabin/baja/{iddiag}','ResultadoController@del_resgabin')->name('del.resgabin');

	/**Gestion Personal */
	Route::get('gestión/baja/{idpers}/{opcion}','PersonalController@bajapers')->name('bajapers');// Rutas con parametros al inicio
	Route::get('gestión/{idpers?}/{confirmreg?}','PersonalController@gestionpers')->name('gestionpers');
	Route::post('gestión/registro','PersonalController@regispers')->name('regispers');
	

	/**Reglas */
	Route::post('/conocimiento/gestion','PersonalController@gestionreglas')->name('reglas.gestion');
	Route::get('/conocimiento/visual/{confirmupd?}','PersonalController@visualreglas')->name('reglas.visual');

});

Route::prefix('inicio')->group(function(){
	Route::get('/','InicioController@inicio')->name('inicio.inicio');
	Route::get('/newDiagnostico','InicioController@newDiagnostico')->name('inicio.newDiagnostico');
});


// Route::prefix('hechos')->group(function(){
// 	Route::post('/Predisposiciones','HechosController@hechosPredisposiciones')->name('hechos.predisposiciones');
// 	Route::post('/Sintomas','HechosController@hechosSintomas')->name('hechos.sintomas');
// 	Route::post('/Medicamentos','HechosController@hechosMedicamentos')->name('hechos.medicamentos');
// 	Route::post('/Elementos','HechosController@hechosMedicamentos')->name('hechos.elementos');
// 	Route::post('/SintomasTras','HechosController@hechosSintomasTrastornos')->name('hechos.sintomastrastornos');

// });

// Route::prefix('reglas')->group(function(){
// 	Route::get('/Predisposiciones','MotorController@reglasPredisposiciones')->name('reglas.predisposiciones');
// 	Route::get('/Sintomas/{id}','MotorController@reglasSintomas')->name('reglas.sintomas');
// 	Route::get('/Medicamentos/{id}','MotorController@reglasMedicamentos')->name('reglas.medicamentos');
// 	Route::get('/Elementos/{id}','MotorController@reglasElementos')->name('reglas.elementos');

// 	Route::get('/SintomasTras/{id}', 'MotorController@reglasSintomasTrastornos')->name('reglas.sintomastrastornos');

// 	Route::get('/indicador/{valor}', 'MotorController@indicador')->name('reglas.indicador');
// });

Route::prefix('resultado')->group(function(){
	Route::get('/', 'ResultadoController@resultado')->name('resultado');
	Route::get('/detalle/{iddiag}', 'ResultadoController@detalle')->name('detalle');
	Route::get('/historial', 'ResultadoController@historial')->name('historial');

});

// Rutas PG

Route::prefix('show')->group(function(){
	Route::get('/Sintomas/{ids?}','ShowController@showSintomas')->name('show.sintomas');
	Route::get('/Contexto/{ids}/{idc}','ShowController@showContexto')->name('show.contexto');
	Route::get('/SintomasT/','ShowController@showSintTrastorno')->name('show.sintTras');
});


Route::prefix('reglas')->group(function(){
	Route::get('/Sintomas/{id}','MotorInfController@verifySintomas')->name('verificar.sintomas');
	Route::get('/Contextos/{id}','MotorInfController@verifyContextosSint')->name('verificar.contextos');
	Route::get('/Pesos/{id}','MotorInfController@verifyPesosSint')->name('verificar.pesos');
	Route::get('/Elementos/{id}','MotorInfController@verifyElementos')->name('verificar.elementos');
	Route::get('/Indicador/', 'MotorInfController@verifyIndicador')->name('verificar.indicador');
	Route::get('/SintomasT/', 'MotorInfController@verifySintTras')->name('verificar.sintomasT');
});

Route::prefix('hechos')->group(function(){
	Route::post('/Sintomas','HechosController@hechosSintomas')->name('hechos.sintomas');
	Route::post('/Contexto','HechosController@hechosContexto')->name('hechos.contexto');
	Route::post('/Elementos','HechosController@hechosMedicamentos')->name('hechos.elementos');
	Route::post('/SintomasT','HechosController@hechosSintomasT')->name('hechos.sintomasT');
});

/**Navegacion y Calendario*/

Route::get('nav/back','NavController@backRequests')->name('navb');
Route::get('nav/next','NavController@nextRequests')->name('navn');
Route::get('cal/drop/{id}','NavController@calDropevent')->name('caldrop');
Route::get('cal/get','NavController@calGetevents')->name('calget');
Route::post('cal/new','NavController@calNewevent')->name('calnew');


/**Notificaciones */

Route::get('notif/Markread',function(){
	Auth::guard('personalguard')->user()->unreadNotifications()->get()->markAsRead();
	return redirect()->back();
})->name('markRead');

Route::get('notif/Markread/{idnotif}',function($idnotif){
	Auth::guard('personalguard')->user()->unreadNotifications()->where('id',$idnotif)->first()->markAsRead();
})->name('marksingleRead');