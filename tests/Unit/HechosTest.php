<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\User;
use App\Hecho;

class HechosTest extends TestCase
{
    use DatabaseMigrations;

    public function test_registro_sintoma()
    {
        //mock sintoma input
        $user = factory(User::class)->make();
        $request=[
            'sintid'=>1,
            'likert'=>1
        ];

        $this->actingAs($user);
        //funcion
        //$response=$this->post('/hechos/Sintomas', $request);
        $response=$this->get('/show/Sintomas/0');

        //assert
        $response->assertStatus(200);
        /*$this->assertDatabaseHas('p_hechos', [
                'id' => 1,
                'estado'=>70,
                'user_id'=>999,
                'diag_id'=>1,
                'sinto_id'=>1
            ]);*/

    }
}
