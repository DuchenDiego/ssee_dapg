<!--ORDEN 3-->
@extends('personal.inicio.index')
@section('calenactive',"active")
@section('contentpers')
    <div class="jumbotron">
        <h1>Calendario Académico</h1>
        <h2>Agregue o elimine las fechas en las que se debe llenar los casos</h2>
        <h3>Haga click en una fecha vacía para agregar un intérvalo</h3>
        <h3>Haga click en una fecha con un intérvalo asignado para eliminarlo</h3>
        <br><br><br>

        <div class="row">
           <div class="col-md-12">
                <div id="calendario">

                </div>
           </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-3">
                <h4><i><b>Leyenda de Fechas:</b></i></h4>
            </div>
            <div class="col-md-9"></div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div style="background-color:#F3F3A6;color:#F3F3A6;width:50%;">asdf</div>Verano,Invierno
            </div>
            <div class="col-md-3">
                <div style="background-color:#D0DDF1;color:#D0DDF1;width:50%;">asdf</div>Gestión
            </div>
            <div class="col-md-3">
                <div style="background-color:#F7B8BC;color:#F7B8BC;width:50%;">asdf</div>Examen Parcial
            </div>
            <div class="col-md-3">
                <div style="background-color:#2C736B;color:#2C736B;width:50%;">asdf</div>Periodo de llenado de Casos
            </div>
        </div>

        <form action="{{route('calnew')}}" method="POST">
            {{ csrf_field() }}
            <div class="modal fade"  id="mdlEvento">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Agregue el llenado de Casos para el intérvalo</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Título</label>
                                    <input type="text" name="titulo" id="titulo" class="form-control" value="Llenado de Casos" readonly >
                                </div>
                                <div class="col-md-4">
                                    <label for="">Fecha de Inicio</label>
                                    <input type="text" name="fecha_inicio" id="fecha_inicio" class="form-control" readonly >
                                </div>
                                <div class="col-md-4">
                                    <label for="">Fecha Final</label>
                                    <input type="text" name="fecha_final" id="fecha_final" class="form-control" minlength="" placeholder="Ej: 2019-01-01" pattern="^[0-9]{4}(-[0-9]{2})(-[0-9]{2})?$">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar Fechas</button>
                        </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
        </form>
     </div>

@endsection
@section('style')
    <!--Fullcalendar-->
    <link rel='stylesheet' href='{{asset('plugins/fullcalendar/fullcalendar.css')}}' />
@endsection
@section('jscript')
    {{-- <script src='{{asset('plugins/fullcalendar/lib/jquery.min.js')}}'></script> --}}
    <script src='{{asset('plugins/fullcalendar/lib/moment.min.js')}}'></script>
    <script src='{{asset('plugins/fullcalendar/fullcalendar.js')}}'></script>
    <script src='{{asset('plugins/fullcalendar/locale/es.js')}}'></script>


    <script>
    $(document).ready(function(){
        
        //Obtener datos de eventos
        var eventos={!! json_encode($eventos) !!};

        
        //Configuracion del calendario
        $('#calendario').fullCalendar({
            
            defaultView: 'month',
            height:500,
            themeSystem:'bootstrap3',
            events:eventos,

            dayClick: function(date, jsEvent, view, resourceObj) {

                if (IsDateHasEvent(date)==1) {
                    $("#fecha_inicio").val(date.format());

                    $("#fecha_final").attr("minlength",date);

                    $("#mdlEvento").modal();

                }
            },

            eventClick: function(calEvent, jsEvent, view) {

                if(calEvent.title=="Llenado de Casos"){
                    
                    var conf=confirm('Está seguro que desea eliminar este intérvalo de llenado?');

                    if(conf){
                        var urldrop="http://localhost:8000/cal/drop/"+calEvent.id;
                        window.location.href = urldrop;
                    }
                    
                }
                
            }

        });

        function IsDateHasEvent(date) {//funcion q devuelve el numero de eventos del dia
            var allEvents = [];
            allEvents = $('#calendario').fullCalendar('clientEvents');
            var eventss = $.grep(allEvents, function (v) {

                if (moment(date.format()).isSameOrAfter(v.start) &&
                    moment(date.format()).isSameOrBefore(v.end)) {
                    return true;
                }
            });
            // return event.length>0;
            return eventss.length;
        }   
    });

    </script>
@endsection