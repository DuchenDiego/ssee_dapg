<!--ORDEN 4-->
@extends('personal.inicio.index')
@section('alerta')
    @if ($confirmupd==true)
        <div class="container">
            <div class="row">
                <article class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
                        <b>Aviso:</b>
                        <p>
                            Pesos de síntomas modificados satisfactoriamente
                        </p>
                    </div>
                </article>
            </div>
        </div>	          
    @endif
@endsection
@section('ruleactive', 'active')
    @section('contentpers')
        <h1 class="text-center" style="font-size: 50px;">Administración de Reglas</h1>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well well-sm">
                            <h2>Asistencia para gestión</h2>
                            <br>
                            <ul>
                                <li><h4>En el siguiente panel se encuentran las reglas del sistema experto y sus relaciones</h4></li>
                                <li><h4>Haga click/tap en un nodo para ver la regla relacionada y sus detalles en la parte inferior de la ventana</h4></li>
                                <li><h4>Utilice la rueda del mouse para acercar o alejar el diagrama</h4></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well well-sm">
                            <h2>Leyenda de Nodos</h2>
                            <br>
                            <ul>
                                <li><h4>En el <b style="color:#E2BC3A;">PRIMER NIVEL</b> se encuentra el resultado obtenido</h4></li>
                                <li><h4>En el <b style="color:#0A2D4D;">SEGUNDO NIVEL</b> se encuentran los elementos de la escala de Hamilton</h4></li>
                                <li><h4>En el <b style="color:#9CC0C4;">TERCER NIVEL</b> se encuentran los síntomas de la escala de Hamilton</h4></li>
                                <li><h4>En el <b style="color:#AB001B;">CUARTO NIVEL</b> se encuentran las variables de contexto evaluadas</h4></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading custom6">
                            <h3 class="panel-title">Encadenamiento de Reglas</h3>
                        </div>
                        <div class="panel-body">
                            <div id="visual"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading custom6">
                        <h3 class="panel-title">Detalle de Regla</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5">
                                <br>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" id="sizing-addon1">Codigo de Regla</span>
                                    <input id="codigo" type="text" class="form-control" value="" disabled>
                                </div>
                                <br>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon" id="sizing-addon1">Nombre</span>
                                    <input id="nombre" type="text" class="form-control" value="" disabled>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div id="div_pesos">
                                    <form method="POST" action="{{route('reglas.gestion')}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        
                                        <h3>Síntomas afectados por la variable de contexto</h3>
                                        <h4>Si desea modificar los pesos para la devaluación de los síntomas relacionados con la variable de contexto,
                                        ingrese un nuevo valor en el campo seguido del nombre de síntoma</h4><br>
    
                                        <div id="detalle_pesos"></div>

                                        <br>
                                        <button type="submit" class="btn btn-lg custom6">Modificar Pesos</button>
                                    </form>
                                </div>
                                <div id="div_detalle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        @section('jscript')

        <script type="text/javascript">

            //Definicion arrays para grafo

            var nodes=[];
            var edges=[];

            //array auxiliar para posterior visualizacion de pesos

            var auxpesos=[];

            //Recepcion de Datos

            nodes.push({id:"RES",group:'RESULT',label:"Resultado"});

            var elementos={!! json_encode($Elementos) !!};
            //console.log(elementos);

            for (var keyelem in elementos) {// iterar sobre objetos( en js si es array-foreach, objeto-for in)
                if(elementos.hasOwnProperty(keyelem)){// excluir metadata de objeto prototype

                    var auxele=elementos[keyelem];

                    for (var keysint in auxele) {
                        if (auxele.hasOwnProperty(keysint)) {

                            if(auxele[keysint]["conclusion"]==1){

                                nodes.push({id:auxele[keysint]["codigo"],group:'ELEM',label:auxele[keysint]["name"]}); //añadir elementos
                                edges.push({from:auxele[keysint]["codigo"], to:"RES"});

                            }else{

                                nodes.push({id:auxele[keysint]["name"],group:'SINT',label:auxele[keysint]["name"], codregla:auxele[keysint]["codigo"] }); //añadir sintomas
                                edges.push({from:auxele[keysint]["name"], to:auxele[keysint]["codigo"]});

                            } 
                        }
                    }
                }
            }

            var contextos={!! json_encode($Contextos) !!};
            //console.log(contextos);

            for (var keycontex in contextos) {
                if(contextos.hasOwnProperty(keycontex)){

                    var auxcon=contextos[keycontex];

                    for(var keypesos in auxcon){
                        if(auxcon.hasOwnProperty(keypesos)){

                            if (auxcon[keypesos]["conclusion"]==1) {

                                $sintrel=(auxcon[keypesos]["name"]).substr(2); //variable para obtener el nombre del sintoma relacionado con el contexto
                                edges.push({from:auxcon[keypesos]["codigo"], to:$sintrel});

                                auxpesos.push(auxcon[keypesos]);
                                                                
                            } else {

                                if (auxcon[keypesos]["name"]!="personalidad") {//pendiente agregado
                                    nodes.push({id:auxcon[keypesos]["codigo"],group:'CONTEX',label:auxcon[keypesos]["name"]});//añadir Contextos    
                                }
                                

                            }
                        }
                    }
                }
            }

            // Configuracion Vis.js
            var container=document.getElementById('visual');
            var data= {
                nodes: nodes,
                edges: edges,
            };
            var options = {

                layout:{
                    hierarchical:{
                        enabled:true,
                        direction: 'DU',
                        levelSeparation: 150,
                        nodeSpacing: 250,
                        sortMethod:'directed'
                    }
                },
                physics:{
                    enabled:false
                },
                nodes:{
                    font:{
                        size:20,
                        color:'white',
                        face:'Monospace',
                        background:'gray'
                    },
                    borderWidth:2,
                    shape:'dot'
                },
                width: '100%',
                height: '700px',
                groups: {
                    CONTEX: {
                        color:{
                            background:/*'#B3C3A9'*/ '#AB001B',
                            highlight:/*'rgba(179, 195, 169, 0.719)'*/'rgba(171, 0, 26, 0.438)',
                            border:'black'
                        }       
                    },
                    ELEM: {
                        color:{
                            background:'#0A2D4D',
                            highlight:'rgba(10, 45, 77, 0.486)',
                            border:'white'
                        }
                    },
                    SINT: {
                        color:{
                            background:'#9CC0C4',
                            highlight:'rgba(156, 192, 196, 0.356)',
                            border:'black'
                        }
                    },
                    RESULT: {
                        color:{
                            background:'#E2BC3A',
                            highlight:'rgba(226, 187, 58, 0.507)',
                            border:'black'
                        }
                    }
                }
                
            }
            var network = new vis.Network(container, data, options);

            /*Eventos del grafo*/
            network.on( 'selectNode', function(properties) {
                var nodeId = properties.nodes[0];
                var clickedNode=this.body.nodes[nodeId];

                if (clickedNode.options.group=="RESULT") {
                    $('#codigo').val(clickedNode.options.id);
                    $('#nombre').val(clickedNode.options.label);
                    
                    /*Agregado de elementos html*/

                    $('#div_detalle').append("<h3>Elementos de Escala Relacionados</h3><br>");
                    nodes.forEach(item => {
                        if(item["group"]=="ELEM"){
                            $('#div_detalle').append("<input type='text' class='form-control' value='"+item["label"]+"' disabled>");
                        }
                    });
                }
                else if(clickedNode.options.group=="ELEM"){
                    $('#codigo').val(clickedNode.options.id);
                    $('#nombre').val(clickedNode.options.label);

                    $('#div_detalle').append("<h3>Síntomas Relacionados con Elemento</h3><br>");
                    nodes.forEach(item => {
                        if(item["group"]=="SINT" && item["codregla"]==clickedNode.options.id){
                            $('#div_detalle').append("<input type='text' class='form-control' value='"+item["label"]+"' style='font-size:20px;' disabled>");
                        }
                    });
                }
                else if(clickedNode.options.group=="SINT"){
                    $('#codigo').val(clickedNode.options.codregla);
                    $('#nombre').val(clickedNode.options.label);

                    $('#div_detalle').append("<h3>Síntoma perteneciente al Elemento</h3><br>");
                    var ele_rel;
                    nodes.forEach(item => {
                        if(item["group"]=="SINT" && item["codregla"]==clickedNode.options.codregla){
                            ele_rel=item["codregla"];
                        }
                    });
                    nodes.forEach(itemaux => {
                        if(itemaux["group"]=="ELEM" && itemaux["id"]==ele_rel){
                            $('#div_detalle').append("<input type='text' class='form-control' value='"+itemaux["label"]+"' style='font-size:20px;' disabled>");
                        }
                    });
                }
                else if(clickedNode.options.group=="CONTEX"){
                    $('#codigo').val(clickedNode.options.id);
                    $('#nombre').val(clickedNode.options.label);
                    console.log(auxpesos);
                    $('#div_pesos').show();

                    for (var peso in auxpesos) {
                        if (auxpesos.hasOwnProperty(peso)) {
                            if (auxpesos[peso]["codigo"]==clickedNode.options.id) {
                                $('#detalle_pesos').append(
                                    "<input type='hidden' name='premis_id' value='"+auxpesos[peso]["premis_id"]+"'>"+
                                    "<div class='input-group input-group-lg'>"+
                                        "<span class='input-group-addon' id='sizing-addon1'>"+auxpesos[peso]["name"].substr(2)+"</span>"+
                                        "<span class='input-group-addon'>-</span>"+
                                        "<input name='"+auxpesos[peso]["name"]+"' type='number' class='form-control' value='"+auxpesos[peso]["valor"]+"' min=0 max=100>"+
                                        "<span class='input-group-addon'>%</span>"+
                                    "</div>"
                                );
                            }
                        }
                    }
                }
            });

            network.on('deselectNode', function(){
                $('#div_detalle').empty();
                $('#detalle_pesos').empty();
                $('#div_pesos').hide();
            });

            $(document).ready(function(){
                $('#div_pesos').hide();
            });
        </script> 
       
        @endsection
@endsection