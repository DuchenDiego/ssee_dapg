<!--ORDEN 4-->
@extends('personal.inicio.index')
@section('ruleactive', 'active')
@section('contentpers')
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-7">
                <h1>Gestión de Reglas</h1>
            </div>
            <div class="col-md-2"></div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default ">
                    <div class="panel-heading custom6">Tipo de Regla</div>
                    <div class="panel-body">
                        <label for="busqueda" class="control-label">Seleccionar el tipo de Regla</label>
                        <br><br>
                        <select name="orden" class="form-control" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <option value="Contexto">Variable de Contexto</option>
                            <option value="Sintoma">Síntoma(s) de Elemento</option>
                            <option value="Sinttras">Síntoma(s) de Trastorno</option>
                        </select>     
                    </div>
                    <br>
                </div>
            </div>
            <div class="col-md-6">
                <h3>Especificar el tipo de regla que se quiere ingresar</h3>
                <h3>Luego definir la estructura de la regla y cada una de sus partes</h3>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading custom6">Estructura de Regla</div>
                <div class="panel-body">
                   <div id="FormContexto">
                        {{-- <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}
                            <div class="form-group">
    
                                <div class="col-md-6">
                                    SI <input type="text" class="form-control" name="premisa" value="" required autofocus>
                                    <select name="comparador1" class="form-control">
                                        <option value="">=</option>
                                        <option value="">!=</option>
                                        <option value=""><</option>
                                        <option value="">></option>
                                        <option value="">>=</option>
                                        <option value=""><=</option>
                                    </select>
                                    <input type="text" class="form-control">
                                    <br>
                                    ENTONCES <input type="text" class="form-control">
                                    <select name="comparador2" class="form-control">
                                        <option value="">=</option>
                                        <option value="">!=</option>
                                    </select> 
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </form> --}}

                        <form action="" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="conname"> Ingrese el nombre de la variable de Contexto</label>
                                    <input type="text" class="form-control" placeholder="Ej. indecisión_vocacional" name="conname" value="" required autofocus>
                                    <br>
                                    <label for="conname"> Defina si la variable se evaluará en escala</label>
                                    <br> 
                                    Escala likert  &nbsp; <input type="checkbox" data-toggle="toggle" name="likert" id="likert" autocomplete="off">
                                    
                                </div>
                                <div class="col-md-6">
                                    <label for="con"> Ingrese los valores de la escala</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" disabled placeholder="Ej. Bajo" id="esc1">
                                            <input type="text" class="form-control" disabled placeholder="Ej. Muy Bajo" id="esc2">
                                            <input type="text" class="form-control" disabled placeholder="Ej. Medio" id="esc3">
                                            <input type="text" class="form-control" disabled placeholder="Ej. Alto" id="esc4">
                                            <input type="text" class="form-control" disabled placeholder="Ej. Muy Alto" id="esc5">
                                        </div>
                                        <div class="col-md-6">
                                            <span for="esc1"> Nivel 1</span>
                                            <br>.<br>.<br>.<br>.<br>.<br>.<br>
                                            <span for="esc1"> Nivel 5</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                
                            </div>
                        </form>
                   </div>
                   <div id="FormContexto"></div>
                </div>
            </div>
        </div>
    </div>
    @section('jscript')
        <script>
            $('#likert').change(function(){
                if( $(this).is(':checked') ){
                    $('#esc1').prop('disabled', false);
                    $('#esc2').prop('disabled', false);
                    $('#esc3').prop('disabled', false);
                    $('#esc4').prop('disabled', false);
                    $('#esc5').prop('disabled', false); 
                }else{
                    $('#esc1').prop('disabled', true);
                    $('#esc2').prop('disabled', true);
                    $('#esc3').prop('disabled', true);
                    $('#esc4').prop('disabled', true);
                    $('#esc5').prop('disabled', true);
                    $('#esc1').val("");
                    $('#esc2').val("");
                    $('#esc3').val("");
                    $('#esc4').val("");
                    $('#esc5').val("");  
                };
            });
        </script>
    @endsection
@endsection