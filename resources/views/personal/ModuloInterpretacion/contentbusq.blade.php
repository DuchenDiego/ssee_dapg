<!--ORDEN 4-->
@extends('personal.inicio.index')
@section('detecactive',"active")
@section('contentpers')
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-7">
                <h1>Lista de Estudiantes</h1>
            </div>
            <div class="col-md-2"></div>
        </div>
        <br>
        <div class="row">
            <article class="col-md-6">
                <h3>Ingrese los datos conocidos del estudiante para ver los registros relacionados</h2>
                <div class="panel panel-default">
                    <div class="panel-heading custom7">Búsqueda por parámetros</div>
                    <div class="panel-body">
                        <form action="{{ route('busqueda.espec')}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label for="busqueda" class="col-md-12 control-label">Active las opciones 
                                e ingrese los datos del estudiante, mientras más parámetros ingrese,
                                más precisa será la búsqueda
                            </label>
                                                            
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-3">Opciones </div>
                                <div class="col-md-4">
                                    <input type="checkbox" data-toggle="toggle" name="togglecred" id="togglecred" autocomplete="off" data-width="100">
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" data-toggle="toggle" name="togglenac" id="togglenac" autocomplete="off" data-width="100">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                        <input type="checkbox" data-toggle="toggle" name="togglecar" id="togglecar" autocomplete="off" data-width="100">
                                </div>
                                <div class="col-md-4">
                                        <input type="checkbox" data-toggle="toggle" name="togglesem" id="togglesem" autocomplete="off" data-width="100">
                                </div>
                            </div>
                            <br>
                            <div id="items"><!--Div contenedor de opciones-->
                                <div class="row" id="cred">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        Credencial
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="credencial" name="credencial" class="form-control" placeholder="Ej: dib2015049" maxlength="10">
                                    </div>
                                </div>
                                <div class="row" id="naci">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        Nacimiento
                                    </div>
                                    <div class="col-md-8">
                                        <input id="nacfech" type="text" class="form-control" name="nacfech" placeholder="Ej: 1996-03-15" minlength="10" maxlength="10" pattern="^[0-9]{4}(-[0-9]{2})(-[0-9]{2})?$">
                                    </div>
                                </div>
                                <div class="row" id="carr">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        Carrera
                                    </div>
                                    <div class="col-md-8">
                                        <select id="campocar1" name="campocar1" class="form-control">
                                            <option value="Ing_Sistemas">Ing_Sistemas</option>
                                            <option value="Ing_Electrónica">Ing_Electrónica</option>
                                            <option value="Ing_Telecomunicaciones">Ing_Telecomunicaciones</option>
                                            <option value="Ing_Industrial">Ing_Industrial</option>
                                            <option value="Ing_Biomédica">Ing_Biomédica</option>
                                            <option value="Medicina">Medicina</option>
                                            <option value="Odontología">Odontología</option>
                                            <option value="Bioquímica">Bioquímica</option>
                                            <option value="Fisioterapia">Fisioterapia</option>
                                            <option value="Arquitectura">Arquitectura</option>
                                            <option value="Turismo">Turismo</option>
                                            <option value="Gastronomía">Gastronomía</option>
                                            <option value="Psicología">Psicología</option>
                                            <option value="Ing_Petrolera">Ing_Petrolera</option>
                                            <option value="Ing_Civil">Ing_Civil</option>
                                            <option value="Admin_Empresas">Admin_Empresas</option>
                                            <option value="Comercio_Int">Comercio_Int</option>
                                            <option value="Ing_Comercial">Ing_Comercial</option>
                                            <option value="Derecho">Derecho</option>
                                            <option value="Contaduría_Pública">Contaduría_Pública</option>
                                            <option value="Ing_Financiera">Ing_Financiera</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" id="seme">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        Semestre
                                    </div>
                                    <div class="col-md-8">
                                        <select id="camposem1" name="camposem1" class="form-control">
                                            <option value="1">Primer</option>
                                            <option value="2">Segundo</option>
                                            <option value="3">Tercer</option>
                                            <option value="4">Cuarto</option>
                                            <option value="5">Quinto</option>
                                            <option value="6">Sexto</option>
                                            <option value="7">Séptimo</option>
                                            <option value="8">Octavo</option>
                                            <option value="9">Noveno</option>
                                            <option value="10">Décimo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row" id="busqgen">
                                <article class="col-md-3"></article>
                                <article class="col-md-6">
                                    <button type="submit" class="btn btn-primary btn-block custom8" center>
                                            Buscar
                                    </button>
                                </article>
                                <article class="col-md-3"></article>
                            </div>
                        </form>
                    </div>
                </div>
            </article>
            <article class="col-md-6">
                <h3>También puede buscar estudiante(s) por un campo en común</h3>
                <div class="panel panel-default">
                    <div class="panel-heading custom7">Búsqueda por Campo</div>
                    <div class="panel-body">
                    <form action="{{ route('busqueda.gen') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <article class="col-md-12">
                                    <label for="busqueda" class="control-label">Buscar registros segun el campo seleccionado:</label>
                            </article>
                        </div>
                        <div class="row">
                            <article class="col-md-6">
                                <select id="campo" name="campo" class="form-control">
                                    <option value="General">General</option>
                                    <option value="Carrera">Carrera</option>
                                    <option value="Semestre">Semestre</option>
                                </select>
                            </article>
                            <article class="col-md-6">
                                <select id="campocar2" name="campocar2" class="form-control">
                                    <option value="Ing_Sistemas">Ing_Sistemas</option>
                                    <option value="Ing_Electrónica">Ing_Electrónica</option>
                                    <option value="Ing_Telecomunicaciones">Ing_Telecomunicaciones</option>
                                    <option value="Ing_Industrial">Ing_Industrial</option>
                                    <option value="Ing_Biomédica">Ing_Biomédica</option>
                                    <option value="Medicina">Medicina</option>
                                    <option value="Odontología">Odontología</option>
                                    <option value="Bioquímica">Bioquímica</option>
                                    <option value="Fisioterapia">Fisioterapia</option>
                                    <option value="Arquitectura">Arquitectura</option>
                                    <option value="Turismo">Turismo</option>
                                    <option value="Gastronomía">Gastronomía</option>
                                    <option value="Psicología">Psicología</option>
                                    <option value="Ing_Petrolera">Ing_Petrolera</option>
                                    <option value="Ing_Civil">Ing_Civil</option>
                                    <option value="Admin_Empresas">Admin_Empresas</option>
                                    <option value="Comercio_Int">Comercio_Int</option>
                                    <option value="Ing_Comercial">Ing_Comercial</option>
                                    <option value="Derecho">Derecho</option>
                                    <option value="Contaduría_Pública">Contaduría_Pública</option>
                                    <option value="Ing_Financiera">Ing_Financiera</option>
                                </select>
                                <select id="camposem2" name="camposem2" class="form-control">
                                    <option value="1">Primer</option>
                                    <option value="2">Segundo</option>
                                    <option value="3">Tercer</option>
                                    <option value="4">Cuarto</option>
                                    <option value="5">Quinto</option>
                                    <option value="6">Sexto</option>
                                    <option value="7">Séptimo</option>
                                    <option value="8">Octavo</option>
                                    <option value="9">Noveno</option>
                                    <option value="10">Décimo</option>
                                </select>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                            <article class="col-md-3"></article>
                            <article class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block custom8" center>
                                        Buscar
                                </button>
                            </article>
                            <article class="col-md-3"></article>
                        </div>
                    </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
    @section('jscript')
        <script>
            //carga del documento
            $(document).ready(function(){
                $('#cred').hide();
                $('#naci').hide();
                $('#carr').hide();
                $('#seme').hide();
                //$('#busqgen').hide();
                $('#campocar2').hide();
                $('#camposem2').hide();
            });

            //campos busqueda general
            $('#campo').change(function(){
                
                if(document.getElementById("campo").value=="Carrera"){
                    $('#camposem2').hide();
                    $('#campocar2').fadeIn();
                }else if(document.getElementById("campo").value=="Semestre"){
                    $('#campocar2').hide();
                    $('#camposem2').fadeIn();
                }else{
                    $('#campocar2').fadeOut();
                    $('#camposem2').fadeOut();
                }
            });

            //campos buqueda especifica
            $('#togglecred').change(function(){
                $('#cred').fadeToggle();
                if(($(this).is(':checked'))==false){
                    $('#credencial').val("");
                    $('#credencial').prop('required',false);
                }else{
                    $('#credencial').prop('required',true);
                    
                }
            });
            $('#togglenac').change(function(){
                $('#naci').fadeToggle();
                if(($(this).is(':checked'))==false){
                    $('#nacfech').val("");
                    $('#nacfech').prop('required',false);
                }else{
                    $('#nacfech').prop('required',true);
                }
            });
            $('#togglecar').change(function(){
                $('#carr').fadeToggle();
            });
            $('#togglesem').change(function(){
                $('#seme').fadeToggle();
            });

        </script>
    @endsection
@endsection