<!--ORDEN 6-->
@extends('personal/ModuloInterpretacion/detallepers')
@section('caso')

    <table class="table table-striped table-bordered table-hover">
        @if(array_key_exists('contextos',$data)!=true && 
            array_key_exists('sintomas',$data)!=true  && 
            array_key_exists('elementos',$data)!=true)
            <tr><h3>El caso seleccionado esta vacío, el estudiante debe completar el caso para desplegar sus detalles.</h3></tr>
            <tr><h3>Por favor, escoja otro caso</h3></tr>
        @endif
        @if (array_key_exists('contextos',$data))
            <tr class="custom3">
                <th>Variables de Contexto del estudiante</th>
                <th>Puntuación</th>
                <th>Pregunta</th>
                <th>Valor en Escala Likert</th>
            </tr>
            @foreach ($data['contextos'] as $keycon=>$contex)
            <tr>
                <td>{{str_replace("_"," ",strtoupper($keycon))}}</td>
                <td>{{$contex["valor"]}}</td>
                <td>{{$contex["pregunta"]}}</td>
                <td>
                    @if ($contex["valor"]==10)
                        {{$contex["opciones"]["opc1"]}}
                    @elseif($contex["valor"]==30)
                        {{$contex["opciones"]["opc2"]}}
                    @elseif($contex["valor"]==50)
                        {{$contex["opciones"]["opc3"]}}
                    @elseif($contex["valor"]==70)
                        {{$contex["opciones"]["opc4"]}}
                    @elseif($contex["valor"]==90)
                        {{$contex["opciones"]["opc5"]}}
                    @elseif ($contex["valor"]==0)
                        {{"Negativo"}}
                    @elseif($contex["valor"]==100)
                        {{"Positivo"}}
                    @endif
                </td>
            </tr>              
            @endforeach
        @endif
        @if (array_key_exists('sintomas',$data))
            <tr class="custom3">
                <th>Síntomas del estudiante</th>
                <th>Puntuación</th>
                <th colspan="2">Pregunta</th>
            </tr>
            @foreach ($data['sintomas'] as $keysint=>$sint)
            <tr>
                <td>{{str_replace("_"," ",strtoupper($keysint))}}</td>
                <td>{{$sint["valor"]}}</td>
                <td colspan="2">{{$sint["pregunta"]}}</td>  
            </tr>              
            @endforeach
        @endif
        @if (array_key_exists('elementos',$data))
            <tr class="custom3">
                <th>Elementos de escala Hamilton</th>
                <th>Puntuación</th>
                <th colspan="2">Valor en Escala Hamilton</th>
            </tr>
            @foreach ($data['elementos'] as $keyele=>$ele)
            <tr>
                <td>{{strtoupper($keyele)}}</td>
                <td>{{$ele}}</td> 
                <td colspan="2">
                    @if ($ele==0)
                        {{"Ausente"}}
                    @elseif($ele==1)
                        {{"Leve"}}
                    @elseif($ele==2)
                        {{"Moderado"}}
                    @elseif($ele==3)
                        {{"Grave"}}
                    @else
                        {{"Muy Grave/Incapacitante"}}
                    @endif
                </td> 
            </tr>              
            @endforeach
        @endif
        @if (array_key_exists('sintomasT',$data))
            <tr class="custom3">
                <th>Síntomas de Trastorno</th>
                <th>Puntuación</th>
                <th colspan="2">Pregunta</th>
            </tr>
            @foreach ($data['sintomasT'] as $keysintt=>$sintt)
            <tr>
                <td>{{str_replace("_"," ",strtoupper($keysintt))}}</td>
                <td>{{$sintt["valor"]}}</td>
                <td colspan="2">{{$sintt["pregunta"]}}</td>  
            </tr>              
            @endforeach
        @endif
        
    </table>

@endsection
@section('resultcaso')

    <div class="col-md-12">
        <div class="input-group input-group-lg">
            <div class="row">
                <span class="input-group-addon custom3" id="sizing-addon1">Resultado</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" value="{{$data["resultado"]}}" disabled>
            </div>
        </div>
    </div>    
    <br><br><br><br> 
    <div class="col-md-12">
        <div class="input-group input-group-lg">
            <div class="row">
                <span class="input-group-addon custom3" id="sizing-addon1">Tipo Posible de Trastorno</span>
            </div>
            <div class="row">
                <input type="text" class="form-control" value="{{$data["tipotrastorno"]}}" disabled>
            </div>
        </div>	
    </div>
    <br><br><br><br>
    <div class="col-md-12">
        @if ($data["resultado"]!="indefinido")
            <a class="btn custom6" href="{{route('pdf')}}">Generar Reporte</a>
        @endif
    </div>

@endsection
@section('resultgabinete')
    
    @if ($resgabin==NULL)

        <h4>
            Actualmente no se encuentra registrada una conclusión del gabinete, 
            si desea ingresarla puede hacerlo en los siguientes campos
        </h4>

        @if (Session::get('superuser')==1 && $data["resultado"]!="indefinido")

            <form action="{{route('resgabin')}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="iddiag" value="{{$data['iddiag']}}">
            <input type="hidden" name="baja" value=0>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group input-group-lg">
                            <div class="row">
                                <span class="input-group-addon custom3" id="sizing-addon1">Observaciones</span>
                            </div>
                            <div class="row">
                                <textarea name="observaciones" id="" class="form-control" cols="30" rows="10" style="resize:none;" required></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <div class="input-group input-group-lg">
                        <div class="row">
                            <span class="input-group-addon custom3" id="sizing-addon1">Resultado Gabinete</span>
                        </div>
                        <div class="row">
                            <input name="resultgabin" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>
                <br><br><br><br>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn custom6">Registrar Conclusión Gabinete</button>
                    </div>
                </div>
                
            </form>  

        @endif
    @else
        <!--Optimizar si es posible-->

        <form action="{{route('resgabin')}}" method="POST">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="iddiag" value="{{$data['iddiag']}}">
        <input type="hidden" name="baja" value=0>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group input-group-lg">
                        <div class="row">
                            <span class="input-group-addon custom3" id="sizing-addon1">Observaciones</span>
                        </div>
                        <div class="row">
                            @if (Session::get('idpers')==$resgabin['pers_id'])
                                <textarea name="observaciones" class="form-control" cols="30" rows="10" style="resize:none;" required>{{$resgabin['notasfinales']}}</textarea>
                            @else
                                <textarea name="observaciones" class="form-control" cols="30" rows="10" style="resize:none;" disabled>{{$resgabin['notasfinales']}}</textarea>
                            @endif 
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-12">
                <div class="input-group input-group-lg">
                    <div class="row">
                        <span class="input-group-addon custom3" id="sizing-addon1">Resultado Gabinete</span>
                    </div>
                    <div class="row">
                            @if (Session::get('idpers')==$resgabin['pers_id'])
                                <input name="resultgabin" value="{{$resgabin['resultexperto']}}" type="text" class="form-control" pattern="[A-Za-z]+" required>
                            @else
                                <input name="resultgabin" value="{{$resgabin['resultexperto']}}" type="text" class="form-control" pattern="[A-Za-z]+" disabled>
                            @endif 
                    </div>
                </div>
            </div>
            <br><br><br><br>
            <div class="row">
                <div class="col-md-12">
                    @if (Session::get('idpers')==$resgabin['pers_id'])
                        <button type="submit" class="btn btn-warning">Modificar Conclusión Gabinete</button>
                    @endif
                </div>
            </div>
                
        </form>
        <br>
        @if (Session::get('idpers')==$resgabin['pers_id'])
            <a href="{{route('del.resgabin',['iddiag'=>$data['iddiag']])}}" class="btn custom7" onclick="return confirm('Esta seguro que desea eliminar esta conclusión?')">Eliminar Conclusión Gabinete</a>
        @endif

    @endif
    <h4>
        <b>Nota:</b>sólo los usuarios administradores pueden añadir/modificar/eliminar
        conclusiones del gabinete y sólo cuando se haya obtenido una conclusión
    </h4>
@endsection