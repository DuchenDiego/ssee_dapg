<!--orden 5-->
@extends('personal/ModuloInterpretacion/contentbusq')
@section('busqueda')

	@if ($busq==NULL)
		@section('alerta')
			<div class="container">
				<div class="row">
					<article class="col-md-12">
						<div class="alert alert-info" role="alert">
							<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
							<b>Aviso:</b>
							<p>No se encontraron registros de la búsqueda realizada,
							   espere a que se ingresen datos relacionados o 
							   ingrese nuevos parámetros de búsqueda
							</p>
						</div>
					</article>
				</div>
			</div>	
		@endsection
	@else
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
				<tr class="custom7">
					<th>Credencial</th>
					<th>Carrera</th>
					<th>Semestre</th>
					<th>Fecha Nacimiento</th>
					<th>Número de Casos</th>
					<th>Detalle</th>
				</tr>
				@foreach($busq as $bus) 
				<tr>
					<td>{{ $bus["credencial"] }}</td>
					<td>{{ $bus["carrera"] }}</td>
					<td>{{ $bus["semestre"] }}</td>
					<td>{{ $bus["fechanac"] }}</td>
					<td>{{ $bus["numdiag"] }}</td>
					<td>
						<center><a href="{{ route('detallepers',['estud'=>$bus["idusr"]]) }}" class="btn btn-warning btn-lg">Detalle</a></center>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	@endif

@endsection
