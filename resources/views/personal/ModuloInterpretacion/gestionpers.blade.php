<!--Orden 4-->
@extends('personal.inicio.index')

@if ($confirmreg==1)
    @section('alerta')
        <div class="container">
            <div class="row">
                <article class="col-md-12">
                    <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                        <b>Aviso:</b>
                        <p>Personal de Gabinete registrado/modificado correctamente
                        </p>
                    </div>
                </article>
            </div>
        </div>	
    @endsection
@endif
@section('gestactive',"active")
@section('contentpers')
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h1>Gestión de Personal Gabinete</h1>
            </div>
            <div class="col-md-1"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <p>Seleccione el personal de gabinete requerido para su modificación o cancelación de cuenta</p>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading custom2">Lista de Personal</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Usuario</th>
                                    <th>Administrador</th>
                                    <th>Modificar</th>
                                    <th>Baja</th>
                                </tr>
                                @foreach ($personal as $pers)
                                    @if (!$pers['baja'])
                                        <tr>
                                            <td>{{$pers['name']}}</td>
                                            <td>
                                                @if ($pers['Su']==1)
                                                    Positivo                                            
                                                @else
                                                    Negativo
                                                @endif
                                            </td>
                                            @if ($pers['name']=="gabinete")
                                                <td>
                                                    <button class="btn btn-md custom9" disabled>Selección</button>
                                                </td>
                                                <td>
                                                    <button class="btn btn-md custom9" disabled>Deshabilitar</a>    
                                                </td>
                                            @else
                                                <td>
                                                    <a class="btn btn-md custom9" href="{{route('gestionpers',['idpers'=>$pers['id']])}}">Selección</a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-md custom9" href="{{route('bajapers',['idpers'=>$pers['id'],'opcion'=>0])}}" onclick="return confirm('Esta seguro que desea deshabilitar este usuario?')">Deshabilitar</a>
                                                </td>   
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <p>Ingrese o en su defecto modifique los datos del personal en caso de haber seleccionado alguno </p>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading custom2">Gestión</div>
                    <div class="panel-body">
                        <form class="form" method="POST" action="{{ route('regispers') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="usuario" class="col-md-4 control-label">Usuario</label>
                                <div class="col-md-6">
                                    @if ($selectedpers!=NULL)
                                        <input id="usuario" name="usuario" type="text" class="form-control" required pattern="[A-Za-z0-9]+" value="{{$selectedpers['name']}}">
                                    @else
                                        <input id="usuario" name="usuario" type="text" class="form-control" required pattern="[A-Za-z0-9]+">
                                    @endif
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                                <label for="password" class="col-md-4 control-label">Contraseña</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required minlength="6">
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                                <label for="superus" class="col-md-4 control-label">Administrador</label>
                                <div class="col-md-6">
                                    @if ($selectedpers!=NULL && $selectedpers['Su']==1)
                                        <input id="superus" type="checkbox" class="form-control" name="superus" checked>    
                                    @else
                                        <input id="superus" type="checkbox" class="form-control" name="superus">    
                                    @endif
                                    
                                </div>
                            </div> 
                            <br><br>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    @if ($selectedpers!=NULL)
                                        <input type="hidden" name="id" id="id" value="{{$selectedpers['id']}}">
                                    @endif
                                    <button type="submit" class="btn btn-lg custom9">
                                        Registrar/Modificar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p>Seleccione a un usuario en el panel inferior si desea volver a habilitarlo</p>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading custom2">Lista de Personal Deshabilitado</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Usuario</th>
                                    <th>Administrador</th>
                                    <th>Alta</th>
                                </tr>
                                @foreach ($personal as $pers)
                                    @if ($pers['baja'])
                                            <tr>
                                                <td>{{$pers['name']}}</td>
                                                <td>
                                                    @if ($pers['Su']==1)
                                                        Positivo                                            
                                                    @else
                                                        Negativo
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn btn-md custom9" href="{{route('bajapers',['idpers'=>$pers['id'],'opcion'=>1])}}">Habilitar</a>
                                                </td>   
                                            </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
@endsection