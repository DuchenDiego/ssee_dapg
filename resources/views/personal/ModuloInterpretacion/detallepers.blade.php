<!--ORDEN 5-->
@extends('personal.inicio.index')
@section('alerta')
	@if ($confirmchange!=null)
		<div class="container">
			<div class="row">
				<article class="col-md-12">
				@if ($confirmchange=="mod")
					<div class="alert alert-success" role="alert">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<b>Aviso:</b>
						<p>Conclusion de Gabinete Agregada/Modificada
						</p>
					</div>
				@elseif($confirmchange=="baja")
					<div class="alert alert-info" role="alert">
						<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
						<b>Aviso:</b>
						<p>Conclusion de Gabinete Eliminada
						</p>
					</div>
				@endif
					
				</article>
			</div>
		</div>	
		
	@endif
@endsection
@section('detecactive',"active")
@section('contentpers')
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-7">
                <h1>Lista de Casos</h1>
            </div>
            <div class="col-md-2"></div>
        </div>
        <br>
        <div class="row">
            <article class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading custom7">Datos Estudiante</div>
                    <div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group input-group-lg">
									<span class="input-group-addon" id="sizing-addon1">Credencial</span>
									<input type="text" class="form-control" value="{{$datauser["idcredencial"]}}" disabled>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group input-group-lg">
									<span class="input-group-addon" id="sizing-addon1">Fecha de Nacimiento</span>
									<input type="text" class="form-control" value="{{$datauser["fechanac"]}}" disabled>
								</div>	
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group input-group-lg">
									<span class="input-group-addon" id="sizing-addon1">Carrera</span>
									<input type="text" class="form-control" value="{{$datauser["carrera"]}}" disabled>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group input-group-lg">
									<span class="input-group-addon" id="sizing-addon1">Semestre</span>
									<input type="text" class="form-control" value="{{$datauser["semestre"]}}" disabled>
								</div>	
							</div>
						</div>
                    </div>
                </div>
            </article>
		</div>
		<p>Escoja un caso para desplegar el detalle del mismo</p>
		<div class="row">
			<article class="col-md-3">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading custom7">Selección de Caso</div>
						<div class="panel-body">
							<div class="row">
								<ul class="nav nav-pills nav-stacked">
									@foreach ($diags as $key=>$diag)
										<li role="presentation">
											<a href="{{route('detallecaso', ['iduser'=>$datauser['id'],'iddiag'=>$diag['id']]) }}">Caso {{++$key}}</a>
										</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading custom7">Conclusión</div>
						<div class="panel-body">
							@yield('resultcaso')
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading custom7">Conclusión Gabinete</div>
						<div class="panel-body">
							@yield('resultgabinete')
						</div>
					</div>
				</div>
			</article>
			<article class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-heading custom7">Detalles de Caso</div>
					<div class="panel-body">
						<div class="row">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									@yield('caso')
								</table>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
    </div>
@endsection