<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Detalle Caso</title>
	<!--<link rel="stylesheet" type="text/css" href="css/app.css">-->
	{{-- <link rel="stylesheet" type="text/css" href="css/estilos.css">--}}
	{{-- https://seeklogo.com/images/U/universidad-del-valle-bolivia-logo-801F396DF0-seeklogo.com.png --}}
	<style>
		{{include (public_path().'/css/app.css')}}
	</style> 
	{{-- <script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js" type="text/javascript"></script> --}}
</head>
<body>
	<div class="row">
		<article class="col-xs-7">
			<b>
				<i>
					Gabinete Psicopedagógico <br> UNIVALLE-LA PAZ <br> Teléfono: 2246725-26-27 interno:2138
				</i>
			</b>
		</article>
		<article class="col-xs-5">
			<img style="width: 80%; height: 50%;" src="{{public_path().'/media/univalle.png'}}" >
		</article>
	</div>
	<div class="row">
		<div class="col-xs-6"><h2>Reporte de Caso</h2></div>
		<div class="col-xs-6">
			<p style="color:purple; font-size: smaller;"> La <b>misión</b> institucional es la divulgación y propagación del conocimiento, a través de la cual lograremos nuestra <br> <b>visión</b>: un país con mayor bienestar social</p>	
		</div>
	</div>
	<br>
	<div class="panel panel-info">
		<div class="panel-heading">Datos Estudiante</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Credencial</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdatauser["idcredencial"]}}</span>
					</div>
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Fecha de Nacimiento</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdatauser["fechanac"]}}</span>
					</div>
				</div>
			</div>
			
			<br>
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Carrera</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdatauser["carrera"]}}</span>
					</div>
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Semestre</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdatauser["semestre"]}}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-warning">
		<div class="panel-heading">Datos Caso</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Resultado</b></span>
						@if ($pdfdata["resultado"]=="Sin Ansiedad")
							<span class="input-group-addon list-group-item-success" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["resultado"]}}</span>
						@elseif($pdfdata["resultado"]=="Ansiedad Menor")
							<span class="input-group-addon list-group-item-info" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["resultado"]}}</span>
						@else
							<span class="input-group-addon list-group-item-warning" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["resultado"]}}</span>
						@endif
					</div>
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;" ><b>Fecha</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["fecha"]}}</span>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Tipo de Posible Trastorno</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["tipotrastorno"]}}</span>
					</div>
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;"><b>Hora</b></span>
						<span class="input-group-addon" id="sizing-addon1" style="font-size:medium;">{{$pdfdata["hora"]}}</span>
					</div>
				</div>
			</div>
			<br>
			<div class="table-responsive" style="word-wrap: break-word; ">
				<table class="table table-striped table-bordered" style="table-layout: fixed;">
					<tr>
						<th style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
						Variables de Contexto del estudiante</th>
						<th style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
						Puntuación</th>
						<th style=" width:40% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
						Pregunta</th>
						<th style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
						Valor en Escala Likert</th>
					</tr>
					@foreach ($pdfdata['contextos'] as $keycon=>$contex)
					<tr>
						<td style="white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							{{ str_replace("_"," ",strtoupper($keycon))}}
						</td>
						<td>{{$contex["valor"]}}</td>
						<td style="white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							{{$contex["pregunta"]}}
						</td>
						<td>
							@if ($contex["valor"]==10)
								{{$contex["opciones"]["opc1"]}}
							@elseif($contex["valor"]==30)
								{{$contex["opciones"]["opc2"]}}
							@elseif($contex["valor"]==50)
								{{$contex["opciones"]["opc3"]}}
							@elseif($contex["valor"]==70)
								{{$contex["opciones"]["opc4"]}}
							@elseif($contex["valor"]==90)
								{{$contex["opciones"]["opc5"]}}
							@elseif ($contex["valor"]==0)
								{{"Negativo"}}
							@elseif($contex["valor"]==100)
								{{"Positivo"}}
							@endif
						</td>  
					</tr>              
					@endforeach
					<tr>
						<th style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							Síntomas del estudiante</th>
						<th>Puntuación</th>
						<th colspan="2">Pregunta</th>
					</tr>
					@foreach ($pdfdata['sintomas'] as $keysint=>$sint)
					<tr>
						<td style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							{{ str_replace("_"," ",strtoupper($keysint))}}
						</td>
						<td>{{$sint["valor"]}}</td>
						<td colspan="2" style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							{{$sint["pregunta"]}}
						</td>  
					</tr>              
					@endforeach
					<tr>
						<th style=" width:50% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
							Elementos de escala Hamilton
						</th>
						<th>Puntuación</th>
						<th colspan="2">Valor en Escala Hamilton</th>
					</tr>
					@foreach ($pdfdata['elementos'] as $keyele=>$ele)
					<tr>
						<td>{{strtoupper($keyele)}}</td>
						<td>{{$ele}}</td> 
						<td colspan="2">
							@if ($ele==0)
								{{"Ausente"}}
							@elseif($ele==1)
								{{"Leve"}}
							@elseif($ele==2)
								{{"Moderado"}}
							@elseif($ele==3)
								{{"Grave"}}
							@else
								{{"Muy Grave/Incapacitante"}}
							@endif
						</td> 
					</tr>              
					@endforeach

					@if (array_key_exists('sintomasT',$pdfdata))
						<tr class="custom3">
							<th>Síntomas de Trastorno</th>
							<th>Puntuación</th>
							<th colspan="2">Pregunta</th>
						</tr>
						@foreach ($pdfdata['sintomasT'] as $keysintt=>$sintt)
						<tr>
							<td>{{str_replace("_"," ",strtoupper($keysintt))}}</td>
							<td>{{$sintt["valor"]}}</td>
							<td colspan="2" style=" width:20% white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word">
								{{$sintt["pregunta"]}}
							</td>  
						</tr>              
						@endforeach
					@endif
				</table>
			</div>
			@if ($pdfresultgabin!=NULL)
				<div class="col-xs-1"></div>
				<div class="col-xs-10">
					<div class="well" style="width: 300px; word-break:break-all; word-wrap:break-word;">
						<b>Observaciones Gabinete</b><br>{{$pdfresultgabin["notasfinales"]}} <br><br>
						<b>Resultado Gabinete</b><br>{{$pdfresultgabin["resultexperto"]}}
					</div>
				</div>
				<div class="col-xs-1"></div>
			@endif
		</div>
	</div>
	
	<script>
		// /*Generación de chart*/

		// var datos={!! json_encode($pdfdata) !!};
		// console.log(datos["contextos"]);

		// anychart.onDocumentReady(function(){

		// 	var data_contex={
		// 		header:["Variable de Contexto", "Valor"],
		// 		rows:datos["contextos"]
		// 	};

		// 	var chart_contex=anychart.bar();

		// 	chart_contex.data(data_contex);

		// 	chart_contex.title("Contexto del Estudiante");

		// 	chart_contex.container("container");

		// 	chart_contex.draw();
		// });

	</script>
</body>
</html>