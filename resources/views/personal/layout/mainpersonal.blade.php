<!--ORDEN 1-->
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('plugins/vis/dist/vis.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css')}}">
    <!--Custom-->
    @yield('style')
    <!--Jquery UI-->
        <link href=" {{ asset('plugins/jquery/css/jquery-ui.css') }} " rel="stylesheet">

</head>
<body class="bg">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                       <img src="{{asset('media/logo-univalle.png')}}" class="logo" >
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            @yield('opciones')
                        @else

                        <!--Dropdown de Notificaciones Estudiantes-->
                        <li class="dropdown">

                            <a href="#" style="font-size: 20px" class="glyphicon glyphicon-bell" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if ((auth()->user()->unreadnotifications->count())>0)
                                    {{-- <span class="caret"></span> --}}<span style="font-size:15px" class="badge">{{auth()->user()->unreadnotifications->count()}}</span>
                                    @endif
                            </a>    
                            <ul class="dropdown-menu" role="menu">
                                @if ((auth()->user()->unreadnotifications->count())>0)
                                    <li><a href="{{route('markRead')}}">Marcar todos como leídos</a></li>    
                                @else
                                    <li><a>No hay nuevas notificaciones</a></li> 
                                @endif
                                

                                @foreach (auth()->user()->unreadnotifications as $notification)
                                <li style="background-color: lightcyan">
                                    <a id="notif" name="{{$notification->id}}" href="{{route('detallecaso',['idusr'=>$notification->data['idestud'],'iddiag'=>$notification->data['iddiag']])}}">
                                        Un nuevo caso del estudiante <b>{{$notification->data['cred']}}</b> ha sido ingresado
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        <!---->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Cerrar Sesión
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!--Jquery UI-->
    <script src=" {{ asset('plugins/jquery/js/jquery-3.2.1.js')  }}"></script>
    <script src="{{ asset('plugins/jquery/js/jquery-ui.js') }} "></script>
    <!-- Scripts -->
    <script>
        /*Func para Toggle checkbox*/
        $(function() {
                $('#likert').bootstrapToggle({
                    on: 'Abilitado',
                    off: 'Deshabilitado'
            });

                $('#togglecred').bootstrapToggle({
                    on: 'Credencial',
                    off: 'Credencial',
                    onstyle: 'warning'
            });

                $('#togglenac').bootstrapToggle({
                        on: 'Nacimiento',
                        off: 'Nacimiento',
                        onstyle: 'warning'
            });

                $('#togglecar').bootstrapToggle({
                        on: 'Carrera',
                        off: 'Carrera',
                        onstyle: 'warning'
            });
                $('#togglesem').bootstrapToggle({
                        on: 'Semestre',
                        off: 'Semestre',
                        onstyle: 'warning'
            });
                $('#superus').bootstrapToggle({
                        on: 'Positivo',
                        off: 'Negativo',
                        onstyle: 'warning'
            });
        })
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('plugins/vis/dist/vis.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js')}}"></script>
    @yield('jscript');<!--Llamada a script especifico en vistas-->
    <script>

        $('#notif').on('click',function(){
            var idnotif=document.getElementById("notif").getAttribute("name");
            // var urlmarknotif="{{route('marksingleRead',['idnotif'=>"+idnotif+"])}}";
            var urlmarknotif="http://localhost:8000/notif/Markread/"+idnotif;
            $.ajax({
                url:urlmarknotif
            });
        });
    </script>
</body>
</html>
