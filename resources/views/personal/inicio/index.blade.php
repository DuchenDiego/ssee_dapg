<!--ORDEN 2-->
@section('content')
    @yield('alerta')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                            <ul class="nav nav-tabs nav-justified">
                                <li role="presentation" class=@yield('homeactive')><a href="{{ route('personal.dashboard') }}"><span class="glyphicon glyphicon-home"></span> Inicio</a></li>
                                <li role="presentation" class=@yield('detecactive')><a href="{{ route('interpret') }}"><span class="glyphicon glyphicon-search"></span> Detecciones</a></li>
                                
                                @if (Session::get('superuser')==1)
                                <li role="presentation" class=@yield('ruleactive')><a href="{{ route('reglas.visual') }}"><span class="glyphicon glyphicon-wrench"></span> Árbol de Reglas</a></li>
                                <li role="presentation" class=@yield('calenactive')><a href="{{ route('calget') }}"><span class="glyphicon glyphicon-calendar"></span> Calendario </a></li>
                                <li role="presentation" class=@yield('gestactive')><a href="{{ route('gestionpers') }}"><span class="glyphicon glyphicon-pencil"></span> Gestión Personal</a></li>
                               
                                @endif
                            </ul>
                    </div>
                    
                    <div class="panel-body">
                            @yield('contentpers')
                    </div>
                    @yield('busqueda')
                </div>
            </div>
        </div>
    </div>
    
@endsection
@extends('personal.layout.mainpersonal')