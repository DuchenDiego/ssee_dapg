<!--ORDEN 3-->
@extends('personal.inicio.index')
@section('homeactive',"active")
@section('contentpers')
    <div class="jumbotron">
        <h1>Gabinete Psicopedagógico</h1>
        <h2>Para continuar por favor escoja una de las siguientes opciones </h2>
        <br><br><br>
        <div class="row">
            <div class="col-md-6">
                <p>Consulta y busqueda de casos relacionados con ansiedad en estudiantes y detecciones de síndrome ansioso</p>
                <br>
                <p><a class="btn custom7 btn-xlarge btn-block" href="{{ route('interpret') }}" role="button">Ir a Detecciones</a></p>
            </div>
            <div class="col-md-6">
                @if (Session::get('superuser')==1)
                    <p>Administración de la base de conocimiento para el sistema experto, incluyendo el encadenamiento de reglas</p>
                    <br>
                    <div class="col-md-4"></div>
                    <div class="col-md-5">
                        <p><a class="btn custom6 btn-xlarge btn-block" href="{{ route('reglas.visual') }}" role="button">Ir a Reglas</a></p>
                    </div>
                    <div class="col-md-3"></div> 
                @endif
            </div>
        </div>
     </div>
     
@endsection