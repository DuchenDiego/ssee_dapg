@extends('deteccion.layouts.maindeteccion')
@section('content')
    <div class="central">
      <div class="container-fluid">
        <div class="main row">
          <article class="col-md-2"></article>
          <article class="col-md-8">
          <br><br>
          @if ((!empty($restriccion)) && $restriccion==1)
              @if (!empty($proximo))
                <div class="alert alert-danger" role="alert">
                  <h2 style="text-align: center;">Restricción por Fechas</h2>
                  <p style="text-align: center;">
                    El llenado de casos en el sistema está deshabilitado hasta el siguiente 
                    intérvalo de fechas: <b>{{$proximo["start"]}}</b> al <b>{{$proximo["end"]}}</b>
                  </p>
                </div>
              @else
                <div class="alert alert-danger" role="alert">
                  <h2 style="text-align: center;">Restricción por Fechas</h2>
                  <p style="text-align: center;">
                    El llenado de casos en el sistema está deshabilitado hasta que 
                    el gabinete asigne una nueva fecha
                  </p>
                </div>
              @endif
          @else
            <div class="alert alert-info" role="alert">
              <h2 style="text-align: center;">Bienvenido!</h2>
              <p style="text-align: center;">Por favor escoja una opción</p>
            </div>
          @endif
          <article class="col-md-2"></article>
        </div>
        <br>
        @if ((!empty($restriccion)) && $restriccion==1)
          <div class="row">
            <button class="btn btn-danger btn-lg btn-block" disabled>Iniciar <span class="glyphicon glyphicon-remove"></span></a>
          </div>
        @else
          <div class="row">
            <a href="{{ route('inicio.newDiagnostico') }}" class="btn btn-primary btn-lg btn-block">Iniciar <span class="glyphicon glyphicon-share-alt"></span></a>
          </div>
        @endif
        <br><br><br>
        <div class="row">
          <a href="{{ route('historial') }}" class="btn btn-warning btn-lg btn-block">Historial <span class="glyphicon glyphicon-th-list"></span></a>
        </div>
      </div>
    </div>
@endsection