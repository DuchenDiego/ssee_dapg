@extends('deteccion.layouts.maindeteccion')
@section('content')
<div class="central">
    <div class="container-fluid">
      <div class="main row">
        <article class="col-md-12 text-center">
          <div class="row">
            <br><br>
            <article class="col-md-12">
              <div class="alert alert-info" role="alert">
                Por Favor Responda a las siguientes preguntas
              </div>
              <br>
            </article>
          </div>
          <div class="row">
           <div class="panel-warning">
             <div class="panel-heading font1">
                 CONTEXTO <span class="glyphicon glyphicon-adjust"></span>
            </div>
             <br><br>
             <div class="panel-body">
                 <div class="col-md-12 col-lg-12 ">
                   <div class="form-group">
                      <form action="{{ route('hechos.contexto')}}" method="POST">
                      <input type="hidden" name="contextid" value="{{ $contextid }}">
                      <input type="hidden" name="sintid" value="{{ $sintid }}">
                      <input type="hidden" name="likert" value="{{ $likert }}">
                      <div class="row">
                        <div class="col-md-9 col-lg-9 ">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <h3 class="bg-light font2">
                            {{ $preg }}
                          </h3>
                        </div>
                        @if ($likert!=1)
                        <div class="col-md-3 col-lg-3 ">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-success btn-lg">
                                <input type="radio" name="si" id="option1" autocomplete="off" > Si <span class="glyphicon glyphicon-ok"></span>
                                </label>
                                <label class="btn btn-danger btn-lg active">
                                <input type="radio" name="no" id="option2" autocomplete="off" > No <span class="glyphicon glyphicon-remove"></span>
                                </label>
                            </div>
                            </div>
                        </div>                           
                        @else
                          {{-- <div class="row">
                            <div class="col-md-9 col-lg-9">
                              <p>@yield('infoextra')</p>
                            </div>
                            <div class="col-md-3 col-lg-3"></div>
                          </div> --}}
                          <br><br><br>
                          <br><br><br>
                          <div class="row">
                            {{-- <div class="col-md-1 col-lg-1"></div> --}}
                            <div class="col-md-12 col-lg-12">
                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                    <label class="btn custom11">
                                    <input style="font-size: 20px" type="radio" name="op1" autocomplete="off">{{ $opc1 }}
                                    </label>
                                    <label class="btn custom10">
                                    <input style="font-size: 20px" type="radio" name="op2" autocomplete="off">{{ $opc2 }}
                                    </label>
                                    <label class="btn custom3">
                                    <input style="font-size: 20px" type="radio" name="op3" autocomplete="off">{{ $opc3 }}
                                    </label>
                                    <label class="btn custom4">
                                    <input style="font-size: 20px" type="radio" name="op4" autocomplete="off">{{ $opc4 }}
                                    </label>
                                    <label class="btn custom5">
                                    <input style="font-size: 20px" type="radio" name="op5" autocomplete="off">{{ $opc5 }}
                                    </label>
                                </div>
                            </div>
                            {{-- <div class="col-md-1 col-lg-1"></div> --}}
                          </div>
                        @endif
                          <div class="row">
                            <br><br><br>
                            <div class="col-md-12 col-lg-12">
                              <button type="submit" class="btn btn-primary btn-lg btn-block">Siguiente <span class="glyphicon glyphicon-menu-right"></span></button> 
                            </div> 
                          </div>
                      </form>
                 </div>
               </div>
             </div>
           </div>
         </div>
        </article>
      </div>
    </div>
  </div>
@endsection   