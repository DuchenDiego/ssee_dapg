@extends('deteccion.layouts.maindeteccion')
@section('content')
    <div class="central-mod">
        <div class="container-fluid">
          <div class="main row">
            <br><br><br><br><br>
            <article class="col-md-12 text-center">
              <div class="row">
              <div class="panel-default">
                <div class="panel-heading font1 custom9">
                    RESULTADO <span class="glyphicon glyphicon-th-list"></span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-lg-12 ">
                      <div class="row">

                          <h3>Datos Estudiante</h3>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Credencial</span>
                                <input type="text" class="form-control" value="{{$user->idcredencial}}" disabled>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Fecha de Nacimiento</span>
                                <input type="text" class="form-control" value="{{$user->fechanac }}" disabled>
                              </div>	
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Carrera</span>
                                <input type="text" class="form-control" value="{{$user->carrera}}" disabled>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Semestre</span>
                                <input type="text" class="form-control" value="{{$user->semestre}}" disabled>
                              </div>	
                            </div>
                          </div>
                          <br>
                          <h3>Datos Caso</h3>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Fecha</span>
                                <input type="text" class="form-control" value="{{$diag->fecha }}" disabled>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Hora</span>
                                <input type="text" class="form-control" value="{{$diag->hora }}" disabled>
                              </div>	
                            </div>
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Resultado</span>
                                <input type="text" class="form-control" value="{{$diag->resultado}}" disabled>
                              </div>
                            </div>
                            {{-- @if ($diag->tipotrastorno!="NA") --}}
                            {{-- <div class="col-md-6">
                              <div class="input-group input-group-lg">
                                <span class="input-group-addon custom9" id="sizing-addon1">Tipo de posible trastorno</span>
                                @if ($diag->tipotrastorno=="TAG")
                                <input type="text" class="form-control" value="Trastorno de Ansiedad Generalizada" disabled>
                                @elseif($diag->tipotrastorno=="TP")
                                <input type="text" class="form-control" value="Trastorno de Pánico" disabled>
                                @elseif($diag->tipotrastorno=="TAS")
                                <input type="text" class="form-control" value="Trastorno de Ansiedad Social" disabled>
                                @elseif($diag->tipotrastorno=="TOC")
                                <input type="text" class="form-control" value="Trastorno Obsesivo Compulsivo" disabled>
                                @elseif($diag->tipotrastorno=="Indefinido")
                                <input type="text" class="form-control" value="Indefinido" disabled>
                                @endif
                              </div>	
                            </div>     --}}
                            {{-- @endif --}}
                          </div>
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              @if ($diag->resultado=="Sin Ansiedad")
                                <div class="alert alert-success" role="alert">
                                  <h4>
                                    <span class="glyphicon glyphicon-thumbs-up"></span> 
                                    Usted no presenta Ansiedad
                                  </h4>
                                </div>
                              @endif
                              @if ($diag->resultado=="Ansiedad Menor")
                                <div class="alert alert-info" role="alert">
                                  <h4>
                                    <span class="glyphicon glyphicon-info-sign"></span> 
                                    Usted presenta Ansiedad Leve, se le recomienda consultar al gabinete psicopedagógico 
                                  </h4>   
                                </div>
                              @endif
                              @if ($diag->resultado=="Síndrome Ansioso")
                                <div class="alert alert-warning" role="alert">
                                  <h4>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span> 
                                    Usted presenta Síndrome Ansioso, debe asistir al gabinete psicopedagógico 
                                  </h4> 
                                </div>
                              @endif
                            </div>
                            <div class="col-md-6"><b>CASO FINALIZADO</b>  
                              <h4>Para salir del sistema, cierre la sesión en la parte superior derecha de la pantalla</h4>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </article>
          </div>
        </div>
      </div>


    {{-- 
        <div class="row">
          <article class="col-md-6"></article>
          <article class="col-md-6">
            <a href="{{ route('detalle', ['iddiag'=>$diag->id]) }}" class="btn btn-warning btn-lg btn-block">Detalles</a>
          </article>
        </div>
      </div>
    </div> --}}
@endsection