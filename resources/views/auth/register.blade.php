@extends('deteccion.layouts.maindeteccion')
@section('opciones')
<li><a href="{{ route('login') }}">Ingreso Estudiantes</a></li>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading custom9">Registro de Estudiante</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('carrera') ? ' has-error' : '' }}">
                            <label for="carrera" class="col-md-4 control-label">Carrera</label>

                            <div class="col-md-6">

                                <select name="carrera" class="form-control">
                                  <option value="Ing_Sistemas">Ing. de Sistemas</option>
                                  <option value="Ing_Electronica">Ing. Electrónica</option>
                                  <option value="Ing_Telecomunicaciones">Ing. de Telecomunicaciones</option>
                                  <option value="Ing_Industrial">Ing. Industrial</option>
                                  <option value="Ing_Biomedica">Ing. Biomédica</option>
                                  <option value="Medicina">Medicina</option>
                                  <option value="Odontologia">Odontología</option>
                                  <option value="Bioquimica">Bioquimica</option>
                                  <option value="Fisioterapia">Fisioterapia</option>
                                  <option value="Arquitectura">Arquitectura</option>
                                  <option value="Turismo">Turismo</option>
                                  <option value="Gastronomia">Gastronomía</option>
                                  <option value="Psicologia">Psicología</option>
                                  <option value="Ing_Petrolera">Ing. Petrolera</option>
                                  <option value="Ing_Civil">Ing. Civil</option>
                                  <option value="Admin_Empresas">Administración de Empresas</option>
                                  <option value="Comercio_Int">Comercio Internacional</option>
                                  <option value="Ing_Comercial">Ing_Comercial</option>
                                  <option value="Derecho">Derecho</option>
                                  <option value="Contaduria_Publica">Contaduría Pública</option>
                                  <option value="Ing_Financiera">Ing. Financiera</option>
                                </select>

                                @if ($errors->has('carrera'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('carrera') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('semestre') ? ' has-error' : '' }}">
                            <label for="semestre" class="col-md-4 control-label">Semestre</label>

                            <div class="col-md-6">

                                <select name="semestre" class="form-control">
                                  <option value="1">1er</option>
                                  <option value="2">2do</option>
                                  <option value="3">3er</option>
                                  <option value="4">4to</option>
                                  <option value="5">5to</option>
                                  <option value="6">6to</option>
                                  <option value="7">7mo</option>
                                  <option value="8">8vo</option>
                                  <option value="9">9no</option>
                                  <option value="10">10mo</option>
                                </select>

                                @if ($errors->has('semestre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('semestre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fechanac') ? ' has-error' : '' }}">
                            <label for="fechanac" class="col-md-4 control-label">Fecha de Nacimiento</label>

                            <div class="col-md-6">
                                <input id="fechanac" type="text" class="form-control" name="fechanac" value="{{ old('fechanac') }}" required autofocus>
                                @if ($errors->has('fechanac'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fechanac') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('idcredencial') ? ' has-error' : '' }}">
                            <label for="idcredencial" class="col-md-4 control-label">Cuenta Credencial</label>

                            <div class="col-md-6">
                                <input id="idcredencial" type="text" class="form-control" name="idcredencial" value="{{ old('idcredencial') }}" placeholder="Ej: dib2015049" pattern="[a-z]{3}[0-9]{7}" minlength="10" maxlength="10" required>

                                @if ($errors->has('idcredencial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idcredencial') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-lg custom9">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
