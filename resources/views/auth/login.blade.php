@extends('deteccion.layouts.maindeteccion')
@section('custstyle')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
@endsection
@section('opciones')
<li><a href="{{ route('personal.login') }}">Gabinete Psicopedagógico</a></li>
<li><a href="{{ route('register') }}">Registro de Estudiante</a></li>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default animated fadeIn">
                <div class="panel-heading custom9">Inicio de Sesión Estudiantes</div>
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <img style="width:90%;" src="{{asset('media/univalle.png')}}" >
                        </div>
                        <div class="col-md-7">
                            <h2 class="font1"><i>Sistema Experto para Detección de Ansiedad</i></h2>
                            <h4><b><i>Ingreso de Estudiantes</i></b></h4>
                        </div>
                    </div>
                    <br>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('idcredencial') ? ' has-error' : '' }}">
                            <label for="idcredencial" class="col-md-4 control-label">CuentaCredencial</label>

                            <div class="col-md-6">
                                <input id="idcredencial" type="text" class="form-control" name="idcredencial" value="{{ old('idcredencial') }}" placeholder="Ej: dib2015049" pattern="[a-z]{3}[0-9]{7}" minlength="10" maxlength="10" required autofocus>

                                @if ($errors->has('idcredencial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idcredencial') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">
                                <button type="submit" class="btn custom9 btn-lg">
                                    Ingresar
                                </button>
                            </div>
                            <div class="col-md-5">
                                <b>No dispone de una cuenta?</b><br>
                                Realize su registro en la barra superior
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection