@extends('deteccion.layouts.maindeteccion')
@section('opciones')
<li><a href="{{ route('login') }}">Login Estudiantes</a></li>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading custom9">Personal Login</div>

                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <img style="width:90%;" src="{{asset('media/univalle.png')}}" >
                        </div>
                        <div class="col-md-7">
                            <h2 class="font1"><i>Sistema Experto para Detección de Ansiedad</i></h2>
                            <h4><b><i>Ingreso de Personal de Gabinete</i></b></h4>
                        </div>
                    </div>
                    <br>
                    <form class="form-horizontal" method="POST" action="{{ route('personal.login.submit') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Usuario</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required pattern="[A-Za-z0-9]+" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn custom9 btn-lg">
                                    Ingreso
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection