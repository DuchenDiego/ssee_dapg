@section('content')  
   @extends('criterios/sintomastrastornos/template_sinttras/contentsinttrasaux')
   @section('pregunta','Siente un constante temor a que se le juzgue o evalúe en situaciones sociales o actos públicos?')
   @section('infoextra','(Discursos, Exposiciones, Reuniones, Fiestas, Charlas)')
@endsection
@extends('layouts.app')