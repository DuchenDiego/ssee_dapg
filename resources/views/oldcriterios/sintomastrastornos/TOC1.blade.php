@section('content')  
   @extends('criterios/sintomastrastornos/template_sinttras/contentsinttrasaux')
   @section('pregunta','Siente que tiene preocupaciones o pensamientos negativos acerca de situaciones desfavorables y por ende realiza actos repetitivos para prevenirlas?')
   @section('infoextra')
   Ejemplos: <br>
   -Debido al pensamiento de contraer una infección, el individuo se lava las manos de forma excesiva<br>
   -Debido al pensamiento de exponer su casa, el individuo comprueba de forma excesiva si la aseguró correctamente<br>
   -Debido al pensamiento de que pueda ocurrir un accidente, el individuo siempre toma la misma ruta para ir a estudiar o trabajar
   @endsection
@endsection
@extends('layouts.app')