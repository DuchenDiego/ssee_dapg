@section('content')  
   @extends('criterios/medicinas/template_med/contentmed')
   @section('pregunta','Usted consumió ultimamente o consume actualmente antiinfecciosos debido a un tratamiento o alguna otra razón?')
   @section('tratamientos','(Ejemplos de tratamientos implicados de los antiinfecciosos: Tratamiento para infección de garganta, Tratamiento para la Neumonía)')
@endsection
@extends('layouts.app')