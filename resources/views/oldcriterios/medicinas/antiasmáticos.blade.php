@section('content')  
   @extends('criterios/medicinas/template_med/contentmedaux')
   @section('pregunta','Usted consumió ultimamente o consume actualmente antiasmáticos debido a un tratamiento o alguna otra razón?')
   @section('tratamientos','(Ejemplos de tratamientos implicados de los antiasmáticos: Tratamiento del asma, dilatación de bronquios para tratamientos alérgicos)')
@endsection
@extends('deteccion.layouts.maindeteccion')