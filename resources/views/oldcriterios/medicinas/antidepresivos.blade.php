@section('content')  
   @extends('criterios/medicinas/template_med/contentmed')
   @section('pregunta','Usted consumió ultimamente o consume actualmente antidepresivos debido a un tratamiento o alguna otra razón?')
   @section('tratamientos','(Ejemplos de tratamientos implicados de los antidepresivos: Tratamiento de depresión y trastornos relacionados)')
@endsection
@extends('layouts.app')